/**
 * 
 */


window.onload = function() {
	document.getElementById("submitbutton").addEventListener('click', submitForm);
};

function putMessage(message) {
	document.getElementById("message").innerText=message;
};

function submitForm() {
	event.preventDefault();

	let form = document.getElementById("formSignIn");
	let formData = new FormData(form);
	let userName = document.getElementById('ersUserName').value;
	
	fetch("/users/passwords",
	    {
	        body: formData,
	        method: "post"
	    }).then(function(response) {
			if (response.status == 200) {
				window.location.href = `/html/home.html?ersUsername=${userName}`;
			} else {
				putMessage("User name or password incorrect, please try again");
			}
	});

};