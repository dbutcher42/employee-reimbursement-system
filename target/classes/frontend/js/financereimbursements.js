/**
 * 
 */


let ersUserId;
let userName;
let userNameJson;
let userRoleId;
let userFirstName;
let userLastName;


window.onload = function() {
	//console.log("window loaded");
	let queryString = window.location.search;
	//console.log(queryString);
	userName = getQueryVariable('ersUsername');
	//console.log(userName);

	let userNameObj = {
		ersUserName: userName  
	};
	
	userNameJson = JSON.stringify(userNameObj);
	//console.log(userNameJson);
	getUser();
	
};

function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
	//console.log(query);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == variable) {
            return decodeURIComponent(pair[1]);
        }
    }
    console.log('Query variable %s not found', variable);
};

function getUser() {
			
	let xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if(xhttp.readyState==4 && xhttp.status==200) {
			let user = JSON.parse(xhttp.responseText);
			ersUserId = user.ersUserId;
			userFirstName = user.userFirstName;
			userLastName = user.userLastName;
			userRoleId = user.userRoleId;
			
			if (userRoleId == 1) {
				tempLink = document.getElementById("managelink");
				document.getElementById('managelink').setAttribute("hidden", "true");
			};
			
			initialList();
			
			document.getElementById("buttonSelectAll").addEventListener('click', initialList);
			document.getElementById("buttonSelectPending").addEventListener('click', loadPending);
			document.getElementById("buttonSelectApproved").addEventListener('click', loadApproved);
			document.getElementById("buttonSelectDenied").addEventListener('click', loadDenied);
			
			document.getElementById("buttonProfile").addEventListener('click', editProfile);
			document.getElementById("buttonPassword").addEventListener('click', editPassword);
			document.getElementById("managelink").addEventListener('click', manageReimbursement);
			document.getElementById("homelink").addEventListener('click', gohome);
			
		};
	};
	xhttp.open("GET", `/users/namedusers/${userName}`);
	xhttp.send();
};	

function initialList() {
	let xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if(xhttp.readyState==4 && xhttp.status==200) {
			let reimbList = JSON.parse(xhttp.responseText);
			append_json(reimbList);
		};
	};
	xhttp.open("GET", `/users/${ersUserId}/managedreimbursements`);
	xhttp.send();
};

function append_json(data){
    let table = document.getElementById('tableBody');
	table.innerHTML = "";
    data.forEach(function(object) {

        let tr = document.createElement('tr');

		let amtVal = object.reimbAmount.toLocaleString(undefined,{ minimumFractionDigits: 2 });
  		let subDate = getFormattedDate(object.reimbSubmitted);
		let resDate = getFormattedDate(object.reimbResolved);

		if (object.reimbStatusId == 1) { 
        	tr.innerHTML = '<td>' + object.reimbId + '</td>' +
        	'<td>' + amtVal + '</td>' +
        	'<td>' + subDate + '</td>' +
			'<td>' + resDate + '</td>' +
			'<td>' + object.typeName + '</td>' +
        	'<td>' + object.statusName + '</td>' +
			`<td><a href="http://localhost:9001/html/reimbursementmanagements.html?ersUserId=${ersUserId}&ersUserName=${userName}&reimbId=${object.reimbId}" class="button  btn btn-sm btn-outline-primary detailsButton">Manage</a></td>`;
		} else {
			tr.innerHTML = '<td>' + object.reimbId + '</td>' +
        	'<td>' + amtVal + '</td>' +
        	'<td>' + subDate + '</td>' +
			'<td>' + resDate + '</td>' +
			'<td>' + object.typeName + '</td>' +
        	'<td>' + object.statusName + '</td>' +
			`<td><a href="http://localhost:9001/html/managedreimbursements.html?ersUserId=${ersUserId}&ersUserName=${userName}&reimbId=${object.reimbId}" class="button  btn btn-sm btn-outline-primary detailsButton">Details</a></td>`;
		};
		table.appendChild(tr);
    });
};

function getFormattedDate(dateIn) {
    
	if (dateIn == null) {
		return "";
	};

	let date = new Date(dateIn);

    let month = date.getMonth() + 1;
    let day = date.getDate();
    let hour = date.getHours();
    let min = date.getMinutes();
    let sec = date.getSeconds();

    month = (month < 10 ? "0" : "") + month;
    day = (day < 10 ? "0" : "") + day;
    hour = (hour < 10 ? "0" : "") + hour;
    min = (min < 10 ? "0" : "") + min;
    sec = (sec < 10 ? "0" : "") + sec;

    let str = date.getFullYear() + "-" + month + "-" + day + " " +  hour + ":" + min + ":" + sec;

    return str;
};

function loadPending() {
	let xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if(xhttp.readyState==4 && xhttp.status==200) {
			let reimbList = JSON.parse(xhttp.responseText);
			append_json(reimbList);
		};
	};
	xhttp.open("GET", `/users/${ersUserId}/managedreimbursements/statuses/1`);
	xhttp.send();
	
};

function loadApproved() {
	let xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if(xhttp.readyState==4 && xhttp.status==200) {
			let reimbList = JSON.parse(xhttp.responseText);
			append_json(reimbList);
		};
	};
	xhttp.open("GET", `/users/${ersUserId}/managedreimbursements/statuses/2`);
	xhttp.send();
};

function loadDenied() {
	let xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if(xhttp.readyState==4 && xhttp.status==200) {
			let reimbList = JSON.parse(xhttp.responseText);
			append_json(reimbList);
		};
	};
	xhttp.open("GET", `/users/${ersUserId}/managedreimbursements/statuses/3`);
	xhttp.send();
};

function editProfile() {
	window.location.href = `/html/profiles.html?ersUserId=${ersUserId}&ersUsername=${userName}`;
};

function editPassword() {
	window.location.href = `/html/newpasswords.html?ersUserId=${ersUserId}&ersUsername=${userName}`;
};

function manageReimbursement() {
	window.location.href = `/html/financereimbursements.html?ersUserId=${ersUserId}&ersUsername=${userName}`;
};

function gohome() {
	window.location.href = `/html/home.html?ersUserId=${ersUserId}&ersUsername=${userName}`;
};