/**
 * 
 */

let ersUserId;
let ersUserName;
let userName;
let userNameJson;
let userRoleId;
let userFirstName;
let userLastName;

window.onload = function() {
	let queryString = window.location.search;
	ersUserId = getQueryVariable('ersUserId');
	ersUserName = getQueryVariable('ersUserName');
	ersReimbId = getQueryVariable('reimbId');
	document.getElementById("updatereimbbutton").addEventListener('click', submitForm);
	let userNameObj = {
		ersUserName: userName  
	};
	
	userNameJson = JSON.stringify(ersUserName);
	getUser();

};

function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == variable) {
            return decodeURIComponent(pair[1]);
        }
    }
    console.log('Query variable %s not found', variable);
};

function submitForm() {
	event.preventDefault();

	let form = document.getElementById("updateReimbursementForm");
	let formData = new FormData(form);
	formData.append('reimbResolver', ersUserId);
	let textComment = document.getElementById("reimbComment").value;
	formData.append('reimbComment', textComment);
	
	fetch(`/users/${ersUserId}/reimbursements/${ersReimbId}`,
    {
        body: formData,
        method: "patch"
    }).then(function(response) {
		if (response.status == 201) {
			window.location.href = `/html/financereimbursements.html?ersUserId=${ersUserId}&ersUsername=${ersUserName}`;
		} else {

		}
	});

};

function getUser() {
			
	let xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if(xhttp.readyState==4 && xhttp.status==200) {
			let user = JSON.parse(xhttp.responseText);
			ersUserId = user.ersUserId;
			userFirstName = user.userFirstName;
			userLastName = user.userLastName;
			userRoleId = user.userRoleId;
			
			if (userRoleId == 1) {
				tempLink = document.getElementById("managelink");
				document.getElementById('managelink').setAttribute("hidden", "true");
			};
			
			document.getElementById("buttonProfile").addEventListener('click', editProfile);
			document.getElementById("buttonPassword").addEventListener('click', editPassword);
			document.getElementById("managelink").addEventListener('click', manageReimbursement);
			document.getElementById("homelink").addEventListener('click', gohome);
			getReimbursement();
			
		};
	};
	xhttp.open("GET", `/users/namedusers/${ersUserName}`);
	xhttp.send();
};	

function editProfile() {
	window.location.href = `/html/profiles.html?ersUserId=${ersUserId}&ersUsername=${ersUserName}`;
};

function editPassword() {
	window.location.href = `/html/newpasswords.html?ersUserId=${ersUserId}&ersUsername=${ersUserName}`;
};

function manageReimbursement() {
	event.preventDefault();
	window.location.href = `/html/financereimbursements.html?ersUserId=${ersUserId}&ersUsername=${ersUserName}`;
};

function gohome() {
	window.location.href = `/html/home.html?ersUserId=${ersUserId}&ersUsername=${ersUserName}`;
};

function getReimbursement() {
	let xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if(xhttp.readyState==4 && xhttp.status==200) {
			let reimb = JSON.parse(xhttp.responseText);
			reimbSubmitted = reimb.reimbSubmitted;
			reimbAmount = reimb.reimbAmount;
			let amtVal = reimbAmount.toLocaleString(undefined,{ minimumFractionDigits: 2 });
			authorName = reimb.authorName;
			reimbDescription = reimb.reimbDescription;
			typeName = reimb.typeName;
			let subDate = getFormattedDate(reimbSubmitted);
			
			document.getElementById("reimbSubmitted").setAttribute("value", subDate);
			document.getElementById("reimbId").setAttribute("value", ersReimbId);	
			document.getElementById("authorName").setAttribute("value", authorName);
			document.getElementById("reimbAmount").setAttribute("value", amtVal);
			document.getElementById('reimbDescription').value = reimbDescription;
			document.getElementById("typeName").setAttribute("value", typeName);
			document.getElementById("cancelbutton").addEventListener('click', manageReimbursement);
			getReceipt();
			
		};
	};
	xhttp.open("GET", `/users/${ersUserName}/reimbursements/${ersReimbId}`);
	xhttp.send();
};

function getReceipt() {
	let xhttp = new XMLHttpRequest();
	xhttp.responseType = "blob";
	xhttp.onreadystatechange = function() {
		if(xhttp.readyState==4 && xhttp.status==200) {
			let receipt = xhttp.response;
			receipt = receipt.slice(0, receipt.size, "image/jpeg")
			
			document.getElementById("receiptImage").src = URL.createObjectURL(receipt);
			
		};
	};
	xhttp.open("GET", `/users/${ersUserId}/reimbursements/${ersReimbId}/receipts`);
	xhttp.send();
};

function getFormattedDate(dateIn) {
    
	if (dateIn == null) {
		return "";
	};

	let date = new Date(dateIn);

    let month = date.getMonth() + 1;
    let day = date.getDate();
    let hour = date.getHours();
    let min = date.getMinutes();
    let sec = date.getSeconds();

    month = (month < 10 ? "0" : "") + month;
    day = (day < 10 ? "0" : "") + day;
    hour = (hour < 10 ? "0" : "") + hour;
    min = (min < 10 ? "0" : "") + min;
    sec = (sec < 10 ? "0" : "") + sec;

    let str = date.getFullYear() + "-" + month + "-" + day + " " +  hour + ":" + min + ":" + sec;

    return str;
};