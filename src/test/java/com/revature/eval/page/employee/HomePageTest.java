package com.revature.eval.page.employee;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.revature.page.HomePage;

public class HomePageTest {
	private static WebDriver driver;
	private HomePage page;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		String filePath = "src/main/resources/chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", filePath);
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		driver.quit();
	}
	
	@Before
	public void setUp() throws Exception {
		this.page = new HomePage(driver, "Manager1");
	}
	
	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testHomePageTableSuccess() throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);

		assertEquals(page.getId(2), "10113");
		assertEquals(page.getAmount(2), "11.11");
		assertEquals(page.getDateSubmitted(2), "2021-01-04 15:58:15");
		assertEquals(page.getDateResolved(2), "2021-01-04 16:00:08");
		assertEquals(page.getType(2), "Lodging");
		assertEquals(page.getStatus(2), "Denied");
	}

	@Test
	public void testAllButtonSuccess() throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);
		page.clickAllButton();
		TimeUnit.SECONDS.sleep(2);

		assertEquals(page.getId(2), "10113");
		assertEquals(page.getAmount(2), "11.11");
		assertEquals(page.getDateSubmitted(2), "2021-01-04 15:58:15");
		assertEquals(page.getDateResolved(2), "2021-01-04 16:00:08");
		assertEquals(page.getType(2), "Lodging");
		assertEquals(page.getStatus(2), "Denied");
	}
	
	@Test
	public void testPendingButtonSuccess() throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);
		page.clickPendingButton();
		TimeUnit.SECONDS.sleep(2);

		assertEquals(page.getId(2), "10116");
		assertEquals(page.getAmount(2), "11.11");
		assertEquals(page.getDateSubmitted(2), "2021-01-04 16:27:50");
		assertEquals(page.getDateResolved(2), "");
		assertEquals(page.getType(2), "Food");
		assertEquals(page.getStatus(2), "Pending");
	}
	
	@Test
	public void testApprovedButtonSuccess() throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);
		page.clickApprovedButton();
		TimeUnit.SECONDS.sleep(2);

		assertEquals(page.getId(2), "10115");
		assertEquals(page.getAmount(2), "11.11");
		assertEquals(page.getDateSubmitted(2), "2021-01-04 15:58:45");
		assertEquals(page.getDateResolved(2), "2021-01-04 15:59:43");
		assertEquals(page.getType(2), "Other");
		assertEquals(page.getStatus(2), "Approved");
	}
	
	@Test
	public void testDeniedButtonSuccess() throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);
		page.clickDeniedButton();
		TimeUnit.SECONDS.sleep(2);

		assertEquals(page.getId(2), "10117");
		assertEquals(page.getAmount(2), "11.11");
		assertEquals(page.getDateSubmitted(2), "2021-01-04 16:28:27");
		assertEquals(page.getDateResolved(2), "2021-01-04 16:28:58");
		assertEquals(page.getType(2), "Other");
		assertEquals(page.getStatus(2), "Denied");
	}

	@Test
	public void testHomeButtonSuccess() throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);
		page.clickHomeButton();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/home.html"));
		assertTrue(driver.getCurrentUrl().equals("http://localhost:9001/html/home.html?ersUserId=9104&ersUsername=Manager1") || 
				driver.getCurrentUrl().equals("http://localhost:9001/html/home.html?ersUserId=9104&ersUsername=Manager1#"));
	}
	
	@Test
	public void testManageButtonSuccess() throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);
		page.clickManageButton();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/financereimbursements.html"));
		assertTrue(driver.getCurrentUrl().equals("http://localhost:9001/html/financereimbursements.html?ersUserId=9104&ersUsername=Manager1") || 
				driver.getCurrentUrl().equals("http://localhost:9001/html/financereimbursements.html?ersUserId=9104&ersUsername=Manager1#"));
	}
	
	@Test
	public void testProfilesButtonSuccess() throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);
		page.clickProfilesButton();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/profiles.html"));
		assertTrue(driver.getCurrentUrl().equals("http://localhost:9001/html/profiles.html?ersUserId=9104&ersUsername=Manager1") || 
				driver.getCurrentUrl().equals("http://localhost:9001/html/profiles.html?ersUserId=9104&ersUsername=Manager1#"));
	}
	
	@Test
	public void testPasswordsButtonSuccess() throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);
		page.clickPasswordsButton();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/newpasswords.html"));
		assertTrue(driver.getCurrentUrl().equals("http://localhost:9001/html/newpasswords.html?ersUserId=9104&ersUsername=Manager1") || 
				driver.getCurrentUrl().equals("http://localhost:9001/html/newpasswords.html?ersUserId=9104&ersUsername=Manager1#"));
	}
	
	@Test
	public void testLogoutButtonSuccess() throws InterruptedException {
		page.clickLogoutButton();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/login.html"));
		assertTrue(driver.getCurrentUrl().equals("http://localhost:9001/html/login.html"));
	}
	
	@Test
	public void testDetailsButtonSuccess() throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);
		page.clickDetailsButton(2);
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/reimbursements.html"));
		assertTrue(driver.getCurrentUrl().equals("http://localhost:9001/html/reimbursements.html?ersUserId=9104&ersUserName=Manager1&reimbId=10113") || 
				driver.getCurrentUrl().equals("http://localhost:9001/html/reimbursements.html?ersUserId=9104&ersUserName=Manager1&reimbId=10113#"));
	}
	
	@Test
	public void testNewReimbursementsButtonSuccess() throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);
		page.clickAddReimbursementButton();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/newreimbursements.html"));
		assertTrue(driver.getCurrentUrl().equals("http://localhost:9001/html/newreimbursements.html?ersUserId=9104&ersUsername=Manager1") || 
				driver.getCurrentUrl().equals("http://localhost:9001/html/newreimbursements.html?ersUserId=9104&ersUsername=Manager1#"));
	}
}
