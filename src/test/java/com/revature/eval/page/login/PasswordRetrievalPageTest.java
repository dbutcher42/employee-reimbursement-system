package com.revature.eval.page.login;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.revature.page.RetrievePasswordPage;

public class PasswordRetrievalPageTest {
	private static WebDriver driver;
	private RetrievePasswordPage page;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		String filePath = "src/main/resources/chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", filePath);
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		driver.quit();
	}
	
	@Before
	public void setUp() throws Exception {
		this.page = new RetrievePasswordPage(driver);
	}
	
	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testfullLoginSuccess() {
		page.setUserName("Employee1");
		page.clickSubmit();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/login.html"));
		assertEquals("http://localhost:9001/html/login.html", driver.getCurrentUrl());
	}
	
	@Test
	public void testfullLoginFailure() throws InterruptedException {
		page.setUserName("Employee55");
		page.clickSubmit();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/passwords.html"));
		TimeUnit.SECONDS.sleep(3);
		assertEquals("http://localhost:9001/html/passwords.html", driver.getCurrentUrl());
		assertEquals(page.getMessage(), "Your user name is invalid");
	}
}
