package com.revature.eval.page.login;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.revature.page.NewUsersPage;

public class NewUsersPageTest {
	private static WebDriver driver;
	private NewUsersPage page;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		String filePath = "src/main/resources/chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", filePath);
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		driver.quit();
	}
	
	@Before
	public void setUp() throws Exception {
		this.page = new NewUsersPage(driver);
	}
	
	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testNewUserSuccess() {
		String generatedCode = "";
		Random rand = new Random();
		for (int i=0; i<8; i++) {
			int randNum = rand.nextInt(26) + 97;
			char newChar = (char) randNum;
			generatedCode = generatedCode + newChar;
		}
		page.setUserName(generatedCode);
		page.setFirstName("John");
		page.setLastName("Conner");
		page.setUserEmail(generatedCode + "@gmail.com");
		page.setRoleId("Finance Manager");
		page.clickSubmitButton();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/login.html"));
		assertEquals("http://localhost:9001/html/login.html", driver.getCurrentUrl());
	}
	
	@Test
	public void testNewUserFailure1() throws InterruptedException {
		page.setUserName("Employee1");
		page.setFirstName("John");
		TimeUnit.SECONDS.sleep(2);
		assertEquals(page.getMessage(), "YOUR USER NAME IS ALREADY TAKEN, PICK ANOTHER");
	}
	
	@Test
	public void testNewUserFailure2() throws InterruptedException {
		page.setUserEmail("johnsmith@gmail.com");
		page.setFirstName("John");
		TimeUnit.SECONDS.sleep(2);
		assertEquals(page.getMessage(), "YOUR EMAIL IS ALREADY REGISTERED, USE ANOTHER");
	}
	
	@Test
	public void testNewUserFailure3() throws InterruptedException {
		page.setUserName("Employee1");
		page.setFirstName("John");
		page.setLastName("Conner");
		page.setUserEmail("rando@gmail.com");
		page.setRoleId("Finance Manager");
		page.clickSubmitButton();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/newusers.html"));
		TimeUnit.SECONDS.sleep(3);
		String message = page.getMessage();
		boolean messageCheck = (message.equals("YOUR USER NAME IS ALREADY TAKEN, PICK ANOTHER") || message.equals("USER NAME OR EMAIL ALREADY TAKEN"));
		assertEquals("http://localhost:9001/html/newusers.html", driver.getCurrentUrl());
		assertTrue(messageCheck);
	}
	
	@Test
	public void testNewUserFailure4() throws InterruptedException {
		page.setUserName("Employee55");
		page.setFirstName("John");
		page.setLastName("Conner");
		page.setUserEmail("johnsmith@gmail.com");
		page.setRoleId("Finance Manager");
		page.clickSubmitButton();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/newusers.html"));
		TimeUnit.SECONDS.sleep(4);
		String message = page.getMessage();
		boolean messageCheck = (message.equals("YOUR USER NAME IS ALREADY TAKEN, PICK ANOTHER") || message.equals("USER NAME OR EMAIL ALREADY TAKEN"));
		assertEquals("http://localhost:9001/html/newusers.html", driver.getCurrentUrl());
		assertTrue(messageCheck);
	}

	@Test
	public void testCancelButtonSuccess() {
		page.clickCancelButton();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/login.html"));
		assertEquals("http://localhost:9001/html/login.html", driver.getCurrentUrl());
	}
}
