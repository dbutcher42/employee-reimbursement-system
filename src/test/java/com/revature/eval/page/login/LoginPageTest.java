package com.revature.eval.page.login;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.revature.page.LoginPage;

public class LoginPageTest {
	private static WebDriver driver;
	private LoginPage page;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		String filePath = "src/main/resources/chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", filePath);
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		driver.quit();
	}
	
	@Before
	public void setUp() throws Exception {
		this.page = new LoginPage(driver);
	}
	
	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testLoginSuccess() {
		page.setUserName("Employee1");
		page.setPassword("password");
		page.clickLoginButton();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/home.html"));
		assertEquals("http://localhost:9001/html/home.html?ersUsername=Employee1", driver.getCurrentUrl());
	}
	
	@Test
	public void testLoginFailure1() throws InterruptedException {
		page.setUserName("Employee1");
		page.setPassword("badpassword");
		page.clickLoginButton();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/login.html"));
		TimeUnit.SECONDS.sleep(3);
		assertEquals("http://localhost:9001/html/login.html", driver.getCurrentUrl());
		assertEquals(page.getMessage(), "User name or password incorrect, please try again");
	}
	
	@Test
	public void testLoginFailure2() throws InterruptedException {
		page.setUserName("Employee55");
		page.setPassword("badpassword");
		page.clickLoginButton();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/login.html"));
		TimeUnit.SECONDS.sleep(3);
		assertEquals("http://localhost:9001/html/login.html", driver.getCurrentUrl());
		assertEquals(page.getMessage(), "User name or password incorrect, please try again");
	}
	
	@Test
	public void testNewUserButtonSuccess() {
		page.clickNewUserButton();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/newusers.html"));
		assertEquals("http://localhost:9001/html/newusers.html", driver.getCurrentUrl());
	}
	
	@Test
	public void testPasswordRetrievalButtonSuccess() {
		page.clickRetrievePasswordButton();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/passwords.html"));
		assertEquals("http://localhost:9001/html/passwords.html", driver.getCurrentUrl());
	}
}
