package com.revature.eval.page.manager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.revature.page.ReimbursementManagementsPage;

public class ReimbursementsManagementsPageTest {
	private static WebDriver driver;
	private ReimbursementManagementsPage page;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		String filePath = "src/main/resources/chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", filePath);
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		driver.quit();
	}
	
	@Before
	public void setUp() throws Exception {
		this.page = new ReimbursementManagementsPage(driver, "Manager1", 9104, 11102);
	}
	
	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testReimbursementManagementsPageTableSuccess() throws InterruptedException {
		TimeUnit.SECONDS.sleep(3);
		assertEquals(page.getreimbId(), "11102");
		assertEquals(page.getAmount(), "25.16");
		assertEquals(page.getDateSubmitted(), "2021-01-05 21:42:39");
		assertEquals(page.getAuthorName(), "John Smith");
		assertEquals(page.getType(), "Food");
		assertEquals(page.getDescription(), "steak and eggs");
		assertTrue(page.isImage());
		
		page.setComments("Comment goes here");
		page.setStatusId("Denied");
		page.clickSubmitButton();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/financereimbursements.html"));
		assertTrue(driver.getCurrentUrl().equals("http://localhost:9001/html/financereimbursements.html?ersUserId=9104&ersUsername=Manager1") || 
				driver.getCurrentUrl().equals("http://localhost:9001/html/financereimbursements.html?ersUserId=9104&ersUsername=Manager1#"));
	}

	@Test
	public void testHomeButtonSuccess() throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);
		page.clickHomeButton();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/home.html"));
		assertTrue(driver.getCurrentUrl().equals("http://localhost:9001/html/home.html?ersUserId=9104&ersUsername=Manager1") || 
				driver.getCurrentUrl().equals("http://localhost:9001/html/home.html?ersUserId=9104&ersUsername=Manager1#"));
	}
	
	@Test
	public void testManageButtonSuccess() throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);
		page.clickManageButton();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/financereimbursements.html"));
		assertTrue(driver.getCurrentUrl().equals("http://localhost:9001/html/financereimbursements.html?ersUserId=9104&ersUsername=Manager1") || 
				driver.getCurrentUrl().equals("http://localhost:9001/html/financereimbursements.html?ersUserId=9104&ersUsername=Manager1#"));
	}
	
	@Test
	public void testProfilesButtonSuccess() throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);
		page.clickProfilesButton();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/profiles.html"));
		assertTrue(driver.getCurrentUrl().equals("http://localhost:9001/html/profiles.html?ersUserId=9104&ersUsername=Manager1") || 
				driver.getCurrentUrl().equals("http://localhost:9001/html/profiles.html?ersUserId=9104&ersUsername=Manager1#"));
	}
	
	@Test
	public void testPasswordsButtonSuccess() throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);
		page.clickPasswordsButton();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/newpasswords.html"));
		assertTrue(driver.getCurrentUrl().equals("http://localhost:9001/html/newpasswords.html?ersUserId=9104&ersUsername=Manager1") || 
				driver.getCurrentUrl().equals("http://localhost:9001/html/newpasswords.html?ersUserId=9104&ersUsername=Manager1#"));
	}
	
	@Test
	public void testLogoutButtonSuccess() throws InterruptedException {
		page.clickLogoutButton();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/login.html"));
		assertTrue(driver.getCurrentUrl().equals("http://localhost:9001/html/login.html"));
	}
	
	@Test
	public void testCancelButtonSuccess() throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);
		page.clickCancelButton();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/financereimbursements.html"));
		assertTrue(driver.getCurrentUrl().equals("http://localhost:9001/html/financereimbursements.html?ersUserId=9104&ersUsername=Manager1") || 
				driver.getCurrentUrl().equals("http://localhost:9001/html/financereimbursements.html?ersUserId=9104&ersUsername=Manager1#"));
	}

}
