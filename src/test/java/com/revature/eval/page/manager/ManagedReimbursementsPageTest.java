package com.revature.eval.page.manager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.revature.page.ManagedReimbursementsPage;

public class ManagedReimbursementsPageTest {
	private static WebDriver driver;
	private ManagedReimbursementsPage page;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		String filePath = "src/main/resources/chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", filePath);
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		driver.quit();
	}
	
	@Before
	public void setUp() throws Exception {
		this.page = new ManagedReimbursementsPage(driver, "Manager1", 9104, 10102);
	}
	
	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testReimbursementsPageTableSuccess() throws InterruptedException {
		TimeUnit.SECONDS.sleep(3);
		assertEquals(page.getreimbId(), "10102");
		assertEquals(page.getAmount(), "1,060.75");
		assertEquals(page.getDateSubmitted(), "2021-01-04 12:08:04");
		assertEquals(page.getDateResolved(), "2021-01-04 12:31:21");
		assertEquals(page.getAuthorName(), "John Smith");
		assertEquals(page.getResolverName(), "John Conner");
		assertEquals(page.getType(), "Travel");
		assertEquals(page.getStatus(), "Approved");
		assertEquals(page.getDescription(), "air plane ticket");
		assertEquals(page.getComments(), "approved");
		assertTrue(page.isImage());
	}

	@Test
	public void testHomeButtonSuccess() throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);
		page.clickHomeButton();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/home.html"));
		assertTrue(driver.getCurrentUrl().equals("http://localhost:9001/html/home.html?ersUserId=9104&ersUsername=Manager1") || 
				driver.getCurrentUrl().equals("http://localhost:9001/html/home.html?ersUserId=9104&ersUsername=Manager1#"));
	}
	
	@Test
	public void testManageButtonSuccess() throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);
		page.clickManageButton();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/financereimbursements.html"));
		assertTrue(driver.getCurrentUrl().equals("http://localhost:9001/html/financereimbursements.html?ersUserId=9104&ersUsername=Manager1") || 
				driver.getCurrentUrl().equals("http://localhost:9001/html/financereimbursements.html?ersUserId=9104&ersUsername=Manager1#"));
	}
	
	@Test
	public void testProfilesButtonSuccess() throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);
		page.clickProfilesButton();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/profiles.html"));
		assertTrue(driver.getCurrentUrl().equals("http://localhost:9001/html/profiles.html?ersUserId=9104&ersUsername=Manager1") || 
				driver.getCurrentUrl().equals("http://localhost:9001/html/profiles.html?ersUserId=9104&ersUsername=Manager1#"));
	}
	
	@Test
	public void testPasswordsButtonSuccess() throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);
		page.clickPasswordsButton();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/newpasswords.html"));
		assertTrue(driver.getCurrentUrl().equals("http://localhost:9001/html/newpasswords.html?ersUserId=9104&ersUsername=Manager1") || 
				driver.getCurrentUrl().equals("http://localhost:9001/html/newpasswords.html?ersUserId=9104&ersUsername=Manager1#"));
	}
	
	@Test
	public void testLogoutButtonSuccess() throws InterruptedException {
		page.clickLogoutButton();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/login.html"));
		assertTrue(driver.getCurrentUrl().equals("http://localhost:9001/html/login.html"));
	}
	
	@Test
	public void testCloseButtonSuccess() throws InterruptedException {
		page.clickCloseButton();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/financereimbursements.html"));
		assertTrue(driver.getCurrentUrl().equals("http://localhost:9001/html/financereimbursements.html?ersUserId=9104&ersUsername=Manager1") || 
				driver.getCurrentUrl().equals("http://localhost:9001/html/financereimbursements.html?ersUserId=9104&ersUsername=Manager1#"));
	}

}
