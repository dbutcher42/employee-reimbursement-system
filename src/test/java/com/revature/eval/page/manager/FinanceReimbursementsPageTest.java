package com.revature.eval.page.manager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.revature.page.FinanceReimbursementsPage;

public class FinanceReimbursementsPageTest {
	private static WebDriver driver;
	private FinanceReimbursementsPage page;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		String filePath = "src/main/resources/chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", filePath);
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		driver.quit();
	}
	
	@Before
	public void setUp() throws Exception {
		this.page = new FinanceReimbursementsPage(driver, "Manager1", 9104);
	}
	
	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testFinanceReimbursementsPageTableSuccess() throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);

		assertEquals(page.getId(2), "10102");
		assertEquals(page.getAmount(2), "1,060.75");
		assertEquals(page.getDateSubmitted(2), "2021-01-04 12:08:04");
		assertEquals(page.getDateResolved(2), "2021-01-04 12:31:21");
		assertEquals(page.getType(2), "Travel");
		assertEquals(page.getStatus(2), "Approved");
	}

	@Test
	public void testAllButtonSuccess() throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);
		page.clickAllButton();
		TimeUnit.SECONDS.sleep(2);

		assertEquals(page.getId(2), "10102");
		assertEquals(page.getAmount(2), "1,060.75");
		assertEquals(page.getDateSubmitted(2), "2021-01-04 12:08:04");
		assertEquals(page.getDateResolved(2), "2021-01-04 12:31:21");
		assertEquals(page.getType(2), "Travel");
		assertEquals(page.getStatus(2), "Approved");
	}
	
	@Test
	public void testPendingButtonSuccess() throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);
		page.clickPendingButton();
		TimeUnit.SECONDS.sleep(2);

		assertEquals(page.getId(2), "10103");
		assertEquals(page.getAmount(2), "145.28");
		assertEquals(page.getDateSubmitted(2), "2021-01-04 12:10:35");
		assertEquals(page.getDateResolved(2), "");
		assertEquals(page.getType(2), "Food");
		assertEquals(page.getStatus(2), "Pending");
	}
	
	@Test
	public void testApprovedButtonSuccess() throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);
		page.clickApprovedButton();
		TimeUnit.SECONDS.sleep(2);

		assertEquals(page.getId(2), "10104");
		assertEquals(page.getAmount(2), "417.41");
		assertEquals(page.getDateSubmitted(2), "2021-01-04 12:11:47");
		assertEquals(page.getDateResolved(2), "2021-01-04 12:31:34");
		assertEquals(page.getType(2), "Lodging");
		assertEquals(page.getStatus(2), "Approved");
	}
	
	@Test
	public void testDeniedButtonSuccess() throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);
		page.clickDeniedButton();
		TimeUnit.SECONDS.sleep(2);

		assertEquals(page.getId(2), "10109");
		assertEquals(page.getAmount(2), "44.79");
		assertEquals(page.getDateSubmitted(2), "2021-01-04 12:28:52");
		assertEquals(page.getDateResolved(2), "2021-01-04 12:32:10");
		assertEquals(page.getType(2), "Food");
		assertEquals(page.getStatus(2), "Denied");
	}

	@Test
	public void testHomeButtonSuccess() throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);
		page.clickHomeButton();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/home.html"));
		assertTrue(driver.getCurrentUrl().equals("http://localhost:9001/html/home.html?ersUserId=9104&ersUsername=Manager1") || 
				driver.getCurrentUrl().equals("http://localhost:9001/html/home.html?ersUserId=9104&ersUsername=Manager1#"));
	}
	
	@Test
	public void testManageButtonSuccess() throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);
		page.clickManageButton();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/financereimbursements.html"));
		assertTrue(driver.getCurrentUrl().equals("http://localhost:9001/html/financereimbursements.html?ersUserId=9104&ersUsername=Manager1") || 
				driver.getCurrentUrl().equals("http://localhost:9001/html/financereimbursements.html?ersUserId=9104&ersUsername=Manager1#"));
	}
	
	@Test
	public void testProfilesButtonSuccess() throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);
		page.clickProfilesButton();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/profiles.html"));
		assertTrue(driver.getCurrentUrl().equals("http://localhost:9001/html/profiles.html?ersUserId=9104&ersUsername=Manager1") || 
				driver.getCurrentUrl().equals("http://localhost:9001/html/profiles.html?ersUserId=9104&ersUsername=Manager1#"));
	}
	
	@Test
	public void testPasswordsButtonSuccess() throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);
		page.clickPasswordsButton();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/newpasswords.html"));
		assertTrue(driver.getCurrentUrl().equals("http://localhost:9001/html/newpasswords.html?ersUserId=9104&ersUsername=Manager1") || 
				driver.getCurrentUrl().equals("http://localhost:9001/html/newpasswords.html?ersUserId=9104&ersUsername=Manager1#"));
	}
	
	@Test
	public void testLogoutButtonSuccess() throws InterruptedException {
		page.clickLogoutButton();
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/login.html"));
		assertTrue(driver.getCurrentUrl().equals("http://localhost:9001/html/login.html"));
	}
	
	@Test
	public void testActionButtonSuccess1() throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);
		page.clickActionButton(1);
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/reimbursementmanagements.html"));
		assertTrue(driver.getCurrentUrl().equals("http://localhost:9001/html/reimbursementmanagements.html?ersUserId=9104&ersUserName=Manager1&reimbId=10101") || 
				driver.getCurrentUrl().equals("http://localhost:9001/html/reimbursementmanagements.html?ersUserId=9104&ersUserName=Manager1&reimbId=10101#"));
	}
	
	@Test
	public void testActionButtonSuccess2() throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);
		page.clickActionButton(2);
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.urlMatches("/managedreimbursements.html"));
		assertTrue(driver.getCurrentUrl().equals("http://localhost:9001/html/managedreimbursements.html?ersUserId=9104&ersUserName=Manager1&reimbId=10102") || 
				driver.getCurrentUrl().equals("http://localhost:9001/html/managedreimbursements.html?ersUserId=9104&ersUserName=Manager1&reimbId=10102#"));
	}

}
