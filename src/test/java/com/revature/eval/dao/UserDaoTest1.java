package com.revature.eval.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.revature.dao.DaoConnection;
import com.revature.dao.UserDao;
import com.revature.model.User;

public class UserDaoTest1 {
	@Mock
	private DaoConnection dc;
	
	@Mock
	private Connection c;
	
	@Mock
	private CallableStatement cs;
	
	@Mock
	private ResultSet rs;
	
	private User testUser;
		
	@BeforeClass
	public static void setupBeforeClass() throws Exception {
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}
	
	@Before
	public void setup() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(dc.getDBConnection()).thenReturn(c);
		when(c.prepareCall(any(String.class))).thenReturn(cs);
		testUser = new User(4,"johndoe1", "password", "John", "Doe", "jd@gmail.com", 2);
		when(rs.first()).thenReturn(true);
		when(rs.getInt(1)).thenReturn(testUser.getErsUserId());
		when(rs.getString(2)).thenReturn(testUser.getErsUserName());
		when(rs.getString(3)).thenReturn(testUser.getErsPassword());
		when(rs.getString(4)).thenReturn(testUser.getUserFirstName());
		when(rs.getString(5)).thenReturn(testUser.getUserLastName());
		when(rs.getString(6)).thenReturn(testUser.getUserEmail());
		when(rs.getInt(7)).thenReturn(testUser.getUserRoleId());
		when(cs.executeQuery()).thenReturn(rs);
		when(cs.executeUpdate()).thenReturn(1);
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void getUserSuccess() {
		assertEquals(new UserDao(dc).getUser(4).getErsUserId(), testUser.getErsUserId());
		assertEquals(new UserDao(dc).getUser(4).getErsUserName(), testUser.getErsUserName());
		assertEquals(new UserDao(dc).getUser(4).getErsPassword(), testUser.getErsPassword());
		assertEquals(new UserDao(dc).getUser(4).getUserFirstName(), testUser.getUserFirstName());
		assertEquals(new UserDao(dc).getUser(4).getUserLastName(), testUser.getUserLastName());
		assertEquals(new UserDao(dc).getUser(4).getUserEmail(), testUser.getUserEmail());
		assertEquals(new UserDao(dc).getUser(4).getUserRoleId(), testUser.getUserRoleId());
	}
	
	@Test
	public void getUserByUserNameSuccess() {
		assertEquals(new UserDao(dc).getUserByUserName("dbutcher42").getErsUserId(), testUser.getErsUserId());
		assertEquals(new UserDao(dc).getUserByUserName("dbutcher42").getErsUserName(), testUser.getErsUserName());
		assertEquals(new UserDao(dc).getUserByUserName("dbutcher42").getErsPassword(), testUser.getErsPassword());
		assertEquals(new UserDao(dc).getUserByUserName("dbutcher42").getUserFirstName(), testUser.getUserFirstName());
		assertEquals(new UserDao(dc).getUserByUserName("dbutcher42").getUserLastName(), testUser.getUserLastName());
		assertEquals(new UserDao(dc).getUserByUserName("dbutcher42").getUserEmail(), testUser.getUserEmail());
		assertEquals(new UserDao(dc).getUserByUserName("dbutcher42").getUserRoleId(), testUser.getUserRoleId());
	}
		
	@Test
	public void insertUserSuccess() {
		assertTrue(new UserDao(dc).insertUser("johndoe1", "password", "John", "Doe", "jd@gmail.com", 2));
	}
	
	@Test
	public void updateUserProfileSuccess() {
		assertTrue(new UserDao(dc).updateUserProfile(4, "Johnny", "Smithe", "jd2@gmail.com", 1));
	}
	
	@Test
	public void updateUserPasswordSuccess() {
		assertTrue(new UserDao(dc).updateUserPassword(4, "password2"));
	}
	
}
