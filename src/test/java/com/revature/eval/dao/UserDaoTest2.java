package com.revature.eval.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.revature.dao.DaoConnection;
import com.revature.dao.UserDao;
import com.revature.model.User;
import com.revature.service.PasswordEncryption;

public class UserDaoTest2 {
	@Mock
	private DaoConnection dc;
	
	@Mock
	private Connection c;
	
	@Mock
	private CallableStatement cs;
	
	@Mock
	private ResultSet rs;
	
	private String tempField;
		
	@BeforeClass
	public static void setupBeforeClass() throws Exception {
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}
	
	@Before
	public void setup() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(dc.getDBConnection()).thenReturn(c);
		when(c.prepareCall(any(String.class))).thenReturn(cs);
		tempField = "dbutcher42@comcast.net";
		when(rs.first()).thenReturn(true);
		when(rs.getString(1)).thenReturn(tempField);
		when(cs.executeQuery()).thenReturn(rs);
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void getUserNameSuccess() {
		assertEquals(new UserDao(dc).getUserName("dbutcher42@comcast.net"), tempField);
	}
	
	@Test
	public void getEmailSuccess() {
		assertEquals(new UserDao(dc).getEmail("dbutcher42@comcast.net"), tempField);
	}
	
}
