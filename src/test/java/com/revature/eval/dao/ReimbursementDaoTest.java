package com.revature.eval.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.revature.dao.DaoConnection;
import com.revature.dao.ReimbursementDao;
import com.revature.model.Reimbursement;
import com.revature.model.ReimbursementEnhanced;

public class ReimbursementDaoTest {
	@Mock
	private DaoConnection dc;
	
	@Mock
	private Connection c;
	
	@Mock
	private CallableStatement cs;
	
	@Mock
	private ResultSet rs;
	
	private ReimbursementEnhanced testReimb;
	private Reimbursement testReimb2;
	private List<ReimbursementEnhanced> reimbList = new ArrayList<ReimbursementEnhanced>();
	private Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	private Blob tempBlob = null;
		
	@BeforeClass
	public static void setupBeforeClass() throws Exception {
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}
	
	@Before
	public void setup() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(dc.getDBConnection()).thenReturn(c);
		when(c.prepareCall(any(String.class))).thenReturn(cs);
		testReimb = new ReimbursementEnhanced(5,150.00, timestamp, timestamp, "So many socks", tempBlob, 1, 0, 1, 4, "None", "Dan", "Bob", "Pending", "Other");
		testReimb2 = new Reimbursement(5,150.00, timestamp, timestamp, "So many socks", tempBlob, 1, 0, 1, 4, "None");
		reimbList.add(testReimb);
		reimbList.add(testReimb);
		when(rs.first()).thenReturn(true);
		when(rs.next()).thenReturn(true).thenReturn(true).thenReturn(false);
		when(rs.getInt(1)).thenReturn(testReimb.getReimbId());
		when(rs.getDouble(2)).thenReturn(testReimb.getReimbAmount());
		when(rs.getTimestamp(3)).thenReturn(testReimb.getReimbSubmitted());
		when(rs.getTimestamp(4)).thenReturn(testReimb.getReimbResolved());
		when(rs.getString(5)).thenReturn(testReimb.getReimbDescription());
		when(rs.getBlob(6)).thenReturn(testReimb.getReimbReceipt());
		when(rs.getInt(7)).thenReturn(testReimb.getReimbAuthor());
		when(rs.getInt(8)).thenReturn(testReimb.getReimbResolver());
		when(rs.getInt(9)).thenReturn(testReimb.getReimbStatusId());
		when(rs.getInt(10)).thenReturn(testReimb.getReimbTypeId());
		when(rs.getString(11)).thenReturn(testReimb.getReimbComment());
		when(rs.getString(12)).thenReturn(testReimb.getAuthorName());
		when(rs.getString(13)).thenReturn(testReimb.getResolverName());
		when(rs.getString(14)).thenReturn(testReimb.getTypeName());
		when(rs.getString(15)).thenReturn(testReimb.getStatusName());
		when(cs.executeQuery()).thenReturn(rs);
		when(cs.executeUpdate()).thenReturn(1);
		when(rs.getBlob(1)).thenReturn(testReimb.getReimbReceipt());
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void getReimbursementAllSuccess() throws SQLException {
		int i =0;
		List<ReimbursementEnhanced> tempList = new ReimbursementDao(dc).getReimbursementAll(1);
		for (ReimbursementEnhanced listItem : tempList) {
			assertEquals(listItem.getReimbId(), reimbList.get(i).getReimbId());
			assertEquals(listItem.getReimbAmount(), reimbList.get(i).getReimbAmount(), 0.0);
			assertEquals(listItem.getReimbSubmitted(), reimbList.get(i).getReimbSubmitted());
			assertEquals(listItem.getReimbResolved(), reimbList.get(i).getReimbResolved());
			assertEquals(listItem.getReimbDescription(), reimbList.get(i).getReimbDescription());
			assertEquals(listItem.getReimbReceipt(), reimbList.get(i).getReimbReceipt());
			assertEquals(listItem.getReimbAuthor(), reimbList.get(i).getReimbAuthor());
			assertEquals(listItem.getReimbResolver(), reimbList.get(i).getReimbResolver());
			assertEquals(listItem.getReimbStatusId(), reimbList.get(i).getReimbStatusId());
			assertEquals(listItem.getReimbTypeId(), reimbList.get(i).getReimbTypeId());
			assertEquals(listItem.getReimbComment(), reimbList.get(i).getReimbComment());
			assertEquals(listItem.getAuthorName(), reimbList.get(i).getAuthorName());
			assertEquals(listItem.getResolverName(), reimbList.get(i).getResolverName());
			assertEquals(listItem.getTypeName(), reimbList.get(i).getTypeName());
			assertEquals(listItem.getStatusName(), reimbList.get(i).getStatusName());
			i++;
		}
	}
	
	@Test
	public void getReimbursementByStatusSuccess() throws SQLException {
		int i =0;
		List<ReimbursementEnhanced> tempList = new ReimbursementDao(dc).getReimbursementByStatus(1,1);
		for (ReimbursementEnhanced listItem : tempList) {
			assertEquals(listItem.getReimbId(), reimbList.get(i).getReimbId());
			assertEquals(listItem.getReimbAmount(), reimbList.get(i).getReimbAmount(), 0.0);
			assertEquals(listItem.getReimbSubmitted(), reimbList.get(i).getReimbSubmitted());
			assertEquals(listItem.getReimbResolved(), reimbList.get(i).getReimbResolved());
			assertEquals(listItem.getReimbDescription(), reimbList.get(i).getReimbDescription());
			assertEquals(listItem.getReimbReceipt(), reimbList.get(i).getReimbReceipt());
			assertEquals(listItem.getReimbAuthor(), reimbList.get(i).getReimbAuthor());
			assertEquals(listItem.getReimbResolver(), reimbList.get(i).getReimbResolver());
			assertEquals(listItem.getReimbStatusId(), reimbList.get(i).getReimbStatusId());
			assertEquals(listItem.getReimbTypeId(), reimbList.get(i).getReimbTypeId());
			assertEquals(listItem.getReimbComment(), reimbList.get(i).getReimbComment());
			assertEquals(listItem.getAuthorName(), reimbList.get(i).getAuthorName());
			assertEquals(listItem.getResolverName(), reimbList.get(i).getResolverName());
			assertEquals(listItem.getTypeName(), reimbList.get(i).getTypeName());
			assertEquals(listItem.getStatusName(), reimbList.get(i).getStatusName());
			i++;
		}
	}
	
	@Test
	public void getReimbursementByAuthorSuccess() throws SQLException {
		int i =0;
		List<ReimbursementEnhanced> tempList = new ReimbursementDao(dc).getReimbursementByAuthor(1);
		for (ReimbursementEnhanced listItem : tempList) {
			assertEquals(listItem.getReimbId(), reimbList.get(i).getReimbId());
			assertEquals(listItem.getReimbAmount(), reimbList.get(i).getReimbAmount(), 0.0);
			assertEquals(listItem.getReimbSubmitted(), reimbList.get(i).getReimbSubmitted());
			assertEquals(listItem.getReimbResolved(), reimbList.get(i).getReimbResolved());
			assertEquals(listItem.getReimbDescription(), reimbList.get(i).getReimbDescription());
			assertEquals(listItem.getReimbReceipt(), reimbList.get(i).getReimbReceipt());
			assertEquals(listItem.getReimbAuthor(), reimbList.get(i).getReimbAuthor());
			assertEquals(listItem.getReimbResolver(), reimbList.get(i).getReimbResolver());
			assertEquals(listItem.getReimbStatusId(), reimbList.get(i).getReimbStatusId());
			assertEquals(listItem.getReimbTypeId(), reimbList.get(i).getReimbTypeId());
			assertEquals(listItem.getReimbComment(), reimbList.get(i).getReimbComment());
			assertEquals(listItem.getAuthorName(), reimbList.get(i).getAuthorName());
			assertEquals(listItem.getResolverName(), reimbList.get(i).getResolverName());
			assertEquals(listItem.getTypeName(), reimbList.get(i).getTypeName());
			assertEquals(listItem.getStatusName(), reimbList.get(i).getStatusName());
			i++;
		}
	}
	
	@Test
	public void getReimbursementByAuthorAndStatusSuccess() throws SQLException {
		int i =0;
		List<ReimbursementEnhanced> tempList = new ReimbursementDao(dc).getReimbursementByAuthorAndStatus(1,1);
		for (ReimbursementEnhanced listItem : tempList) {
			assertEquals(listItem.getReimbId(), reimbList.get(i).getReimbId());
			assertEquals(listItem.getReimbAmount(), reimbList.get(i).getReimbAmount(), 0.0);
			assertEquals(listItem.getReimbSubmitted(), reimbList.get(i).getReimbSubmitted());
			assertEquals(listItem.getReimbResolved(), reimbList.get(i).getReimbResolved());
			assertEquals(listItem.getReimbDescription(), reimbList.get(i).getReimbDescription());
			assertEquals(listItem.getReimbReceipt(), reimbList.get(i).getReimbReceipt());
			assertEquals(listItem.getReimbAuthor(), reimbList.get(i).getReimbAuthor());
			assertEquals(listItem.getReimbResolver(), reimbList.get(i).getReimbResolver());
			assertEquals(listItem.getReimbStatusId(), reimbList.get(i).getReimbStatusId());
			assertEquals(listItem.getReimbTypeId(), reimbList.get(i).getReimbTypeId());
			assertEquals(listItem.getReimbComment(), reimbList.get(i).getReimbComment());
			assertEquals(listItem.getAuthorName(), reimbList.get(i).getAuthorName());
			assertEquals(listItem.getResolverName(), reimbList.get(i).getResolverName());
			assertEquals(listItem.getTypeName(), reimbList.get(i).getTypeName());
			assertEquals(listItem.getStatusName(), reimbList.get(i).getStatusName());
			i++;
		}
	}
	
	@Test
	public void getReimbursementSuccess() {
		assertEquals(new ReimbursementDao(dc).getReimbursement(5).getReimbId(), testReimb.getReimbId());
		assertEquals(new ReimbursementDao(dc).getReimbursement(5).getReimbAmount(), testReimb.getReimbAmount(), 0.0);
		assertEquals(new ReimbursementDao(dc).getReimbursement(5).getReimbSubmitted(), testReimb.getReimbSubmitted());
		assertEquals(new ReimbursementDao(dc).getReimbursement(5).getReimbResolved(), testReimb.getReimbResolved());
		assertEquals(new ReimbursementDao(dc).getReimbursement(5).getReimbDescription(), testReimb.getReimbDescription());
		assertEquals(new ReimbursementDao(dc).getReimbursement(5).getReimbReceipt(), testReimb.getReimbReceipt());
		assertEquals(new ReimbursementDao(dc).getReimbursement(5).getReimbAuthor(), testReimb.getReimbAuthor());
		assertEquals(new ReimbursementDao(dc).getReimbursement(5).getReimbResolver(), testReimb.getReimbResolver());
		assertEquals(new ReimbursementDao(dc).getReimbursement(5).getReimbStatusId(), testReimb.getReimbStatusId());
		assertEquals(new ReimbursementDao(dc).getReimbursement(5).getReimbTypeId(), testReimb.getReimbTypeId());
		assertEquals(new ReimbursementDao(dc).getReimbursement(5).getReimbComment(), testReimb.getReimbComment());
		assertEquals(new ReimbursementDao(dc).getReimbursement(5).getAuthorName(), testReimb.getAuthorName());
		assertEquals(new ReimbursementDao(dc).getReimbursement(5).getResolverName(), testReimb.getResolverName());
		assertEquals(new ReimbursementDao(dc).getReimbursement(5).getTypeName(), testReimb.getTypeName());
		assertEquals(new ReimbursementDao(dc).getReimbursement(5).getStatusName(), testReimb.getStatusName());
	}
	
	@Test
	public void getReceiptSuccess() {
		assertEquals(new ReimbursementDao(dc).getReceipt(50), testReimb.getReimbReceipt());
	}
	
	@Test
	public void insertReimbursementSuccess() {
		assertTrue(new ReimbursementDao(dc).insertReimbursement(150.00, "So many socks", 1, 1, 4));
	}
	
	@Test
	public void updateReimbursementSuccess() {
		assertTrue(new ReimbursementDao(dc).updateReimbursement(4, 2, 3, "None"));
	}
	
	@Test
	public void updateReceiptSuccess() {
		assertTrue(new ReimbursementDao(dc).updateReceipt(50, null));
	}
}
