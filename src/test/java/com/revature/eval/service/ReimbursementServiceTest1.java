package com.revature.eval.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.revature.dao.ReimbursementDao;
import com.revature.model.Reimbursement;
import com.revature.model.ReimbursementEnhanced;
import com.revature.service.ReimbursementService;

public class ReimbursementServiceTest1 {
	
	@Mock
	private ReimbursementDao mockedDao;
	
	private ReimbursementService testService = new ReimbursementService(mockedDao);
	private ReimbursementEnhanced testReimb;
	private Reimbursement testReimb2;
	Timestamp timeStamp = new Timestamp(System.currentTimeMillis());
	private List<ReimbursementEnhanced> reimbList = new ArrayList<ReimbursementEnhanced>();
	
	@BeforeClass
	public static void setupBeforeClass() throws Exception {
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}
	
	@Before
	public void setup() throws Exception {
		MockitoAnnotations.initMocks(this);
		testService = new ReimbursementService(mockedDao);
		testReimb = new ReimbursementEnhanced(50, 150.0, timeStamp, timeStamp, "Socks", null, 1, 1, 1, 1, "None", "Dan", "Bob", "Pending", "Other");
		testReimb2 = new Reimbursement(50, 150.0, timeStamp, timeStamp, "Socks", null, 1, 1, 1, 1, "None");
		reimbList.add(testReimb);
		reimbList.add(testReimb);
		when(mockedDao.insertReimbursement(150.0, "Socks", 1, 1, 1)).thenReturn(true);
		when(mockedDao.getReimbursement(50)).thenReturn(testReimb);
		when(mockedDao.updateReimbursement(50, 1, 2, "None")).thenReturn(true);
		when(mockedDao.getReimbursementAll(1)).thenReturn(reimbList);
		when(mockedDao.getReimbursementByStatus(1,1)).thenReturn(reimbList);
		when(mockedDao.getReimbursementByAuthor(1)).thenReturn(reimbList);
		when(mockedDao.getReimbursementByAuthorAndStatus(1,1)).thenReturn(reimbList);
		when(mockedDao.updateReceipt(50, null)).thenReturn(true);
		when(mockedDao.getReceipt(50)).thenReturn(null);
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void getReimbursementAllSuccess() {
		List<ReimbursementEnhanced> tempList = mockedDao.getReimbursementAll(1);
		boolean check = true;
		if (tempList.isEmpty()) {
			check = false;
		}
		for (int i=0; i<tempList.size(); i++) {
			if (tempList.get(i).equals(reimbList.get(i))) {
				//do nothing
			} else {
				check = false;
			}
		}
		assertTrue(check);
	}
	
	@Test
	public void getReimbursementByStatusSuccess() {
		List<ReimbursementEnhanced> tempList = mockedDao.getReimbursementByStatus(1,1);
		boolean check = true;
		if (tempList.isEmpty()) {
			check = false;
		}
		for (int i=0; i<tempList.size(); i++) {
			if (tempList.get(i).equals(reimbList.get(i))) {
				//do nothing
			} else {
				check = false;
			}
		}
		assertTrue(check);
	}
	
	@Test
	public void getReimbursementByAuthorSuccess() {
		List<ReimbursementEnhanced> tempList = mockedDao.getReimbursementByAuthor(1);
		boolean check = true;
		if (tempList.isEmpty()) {
			check = false;
		}
		for (int i=0; i<tempList.size(); i++) {
			if (tempList.get(i).equals(reimbList.get(i))) {
				//do nothing
			} else {
				check = false;
			}
		}
		assertTrue(check);
	}
	
	@Test
	public void getReimbursementByAuthorAndStatusSuccess() {
		List<ReimbursementEnhanced> tempList = mockedDao.getReimbursementByAuthorAndStatus(1,1);
		boolean check = true;
		if (tempList.isEmpty()) {
			check = false;
		}
		for (int i=0; i<tempList.size(); i++) {
			if (tempList.get(i).equals(reimbList.get(i))) {
				//do nothing
			} else {
				check = false;
			}
		}
		assertTrue(check);
	}
	
	@Test
	public void getReceiptSuccess() {
		assertEquals(testService.getReceipt(50), null);
	}
	
	@Test
	public void addReimbursementSuccess() {
		System.out.println(timeStamp);
		assertTrue(testService.addReimbursement(150.0, "Socks", 1, 1, 1));
	}
	
	@Test
	public void getReimbursementSuccess() {
		assertEquals(testService.getReimbursement(50), testReimb);
	}
	
	@Test
	public void updateReimbursementSuccess() {
		assertTrue(testService.updateReimbursement(50, 1, 2, "None"));
	}
	
	@Test
	public void addReceiptSuccess() {
		assertTrue(testService.addReceipt(50, null));
	}
		
}
