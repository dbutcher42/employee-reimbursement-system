package com.revature.eval.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.revature.dao.UserDao;
import com.revature.model.User;
import com.revature.service.PasswordEncryption;
import com.revature.service.UserService;

public class UserServiceTest1 {
	
	@Mock
	private UserDao mockedDao;
	
	@Mock
	private PasswordEncryption pe; 
	
	@Mock
	private User tempUser;
	
	private UserService testService = new UserService(mockedDao, pe, tempUser);
	private User testUser;
	
	@BeforeClass
	public static void setupBeforeClass() throws Exception {
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}
	
	@Before
	public void setup() throws Exception {
		MockitoAnnotations.initMocks(this);
		testService = new UserService(mockedDao, pe, tempUser);
		testUser = new User(50, "MyUserName", "password", "MyFirstName", "MyLastName", "MyEmail", 1);
		when(tempUser.generatePassword()).thenReturn("password");
		when(pe.encryptPassword(anyString())).thenReturn("password");
		when(pe.decryptPassword(anyString())).thenReturn("password");
		
		when(mockedDao.insertUser("MyUserName", "password", "MyFirstName", "MyLastName", "myemail", 1)).thenReturn(true);
		when(mockedDao.getUser(50)).thenReturn(testUser);
		when(mockedDao.getUserByUserName("MyUserName")).thenReturn(testUser);
		when(mockedDao.updateUserProfile(50, "NewFirst", "NewLast", "newemail", 2)).thenReturn(true);
		when(mockedDao.updateUserPassword(50, "password")).thenReturn(true);
		when(mockedDao.getUserName("MyUserName")).thenReturn("MyUserName");
		when(mockedDao.getEmail("myemail")).thenReturn("myemail");
		when(mockedDao.getPassword("MyUserName")).thenReturn("vSQtpGJf4Moqh/j75vXKvjOOp30YpT7C");
		when(mockedDao.getUserByUserName("Controller1")).thenReturn(testUser);
		when(mockedDao.getPassword("Controller1")).thenReturn("ISF+pz2XPFJJtWwTbhHYMQ1Bn/+luUj5");
		
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void addUserSuccess() {
		assertTrue(testService.addUser("MyUserName", "MyFirstName", "MyLastName", "myemail", 1));
	}
	
	@Test
	public void getUserSuccess() {
		assertEquals(testService.getUser(50), testUser);
	}
	
	@Test
	public void getUserByUserNameSuccess() {
		assertEquals(testService.getUserByUserName("MyUserName"), testUser);
	}
	
	@Test
	public void changeUserProfileSuccess() {
		assertTrue(testService.changeUserProfile(50, "NewFirst", "NewLast", "newemail", 2, "oldemail"));
	}
	
	@Test
	public void changeUserPasswordSuccess() {
		assertTrue(testService.changeUserPassword(50,"password"));
	}
	
	@Test
	public void checkUserNameSuccess() {
		assertTrue(testService.checkUserName("MyUserName"));
	}
	
	@Test
	public void checkEmailSuccess() {
		assertTrue(testService.checkEmail("myemail"));
	}
	
	@Test
	public void checkPasswordSuccess() {
		assertTrue(testService.checkPassword("MyUserName", "password"));
	}
	
	@Test
	public void retrievePasswordSuccess() {
		assertTrue(testService.retrievePassword("Controller1"));
	}
}
