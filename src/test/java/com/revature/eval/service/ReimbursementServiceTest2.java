package com.revature.eval.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.revature.dao.ReimbursementDao;
import com.revature.model.Reimbursement;
import com.revature.model.ReimbursementEnhanced;
import com.revature.service.ReimbursementService;

public class ReimbursementServiceTest2 {
	
	@Mock
	private ReimbursementDao mockedDao;
	
	private ReimbursementService testService = new ReimbursementService(mockedDao);
	private ReimbursementEnhanced testReimb;
	private Reimbursement testReimb2;
	Timestamp timeStamp = new Timestamp(System.currentTimeMillis());
	private List<ReimbursementEnhanced> reimbList = new ArrayList<ReimbursementEnhanced>();
	
	@BeforeClass
	public static void setupBeforeClass() throws Exception {
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}
	
	@Before
	public void setup() throws Exception {
		MockitoAnnotations.initMocks(this);
		testService = new ReimbursementService(mockedDao);
		testReimb = new ReimbursementEnhanced(50, 150.0, timeStamp, timeStamp, "Socks", null, 1, 1, 1, 1, "None", "Dan", "Bob", "Pending", "Other");
		testReimb2 = new Reimbursement(50, 150.0, timeStamp, timeStamp, "Socks", null, 1, 1, 1, 1, "None");
//		reimbList.add(testReimb);
//		reimbList.add(testReimb);
		when(mockedDao.insertReimbursement(150.0, "Socks", 1, 1, 1)).thenReturn(false);
		when(mockedDao.getReimbursement(50)).thenReturn(null);
		when(mockedDao.updateReimbursement(50, 1, 2, "None")).thenReturn(false);
		when(mockedDao.getReimbursementAll(1)).thenReturn(reimbList);
		when(mockedDao.getReimbursementByStatus(1,1)).thenReturn(reimbList);
		when(mockedDao.getReimbursementByAuthor(1)).thenReturn(reimbList);
		when(mockedDao.getReimbursementByAuthorAndStatus(1,1)).thenReturn(reimbList);
		when(mockedDao.updateReceipt(50, null)).thenReturn(false);
		when(mockedDao.getReceipt(50)).thenReturn(null);
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void getReimbursementAllFailure() {
		List<ReimbursementEnhanced> tempList = mockedDao.getReimbursementAll(1);
		boolean check = true;
		if (tempList.isEmpty()) {
			check = false;
		}
		for (int i=0; i<tempList.size(); i++) {
			if (tempList.get(i).equals(reimbList.get(i))) {
				//do nothing
			} else {
				check = false;
			}
		}
		assertFalse(check);
	}
	
	@Test
	public void getReimbursementByStatusFailure() {
		List<ReimbursementEnhanced> tempList = mockedDao.getReimbursementByStatus(1,1);
		boolean check = true;
		if (tempList.isEmpty()) {
			check = false;
		}
		for (int i=0; i<tempList.size(); i++) {
			if (tempList.get(i).equals(reimbList.get(i))) {
				//do nothing
			} else {
				check = false;
			}
		}
		assertFalse(check);
	}
	
	@Test
	public void getReimbursementByAuthorFailure() {
		List<ReimbursementEnhanced> tempList = mockedDao.getReimbursementByAuthor(1);
		boolean check = true;
		if (tempList.isEmpty()) {
			check = false;
		}
		for (int i=0; i<tempList.size(); i++) {
			if (tempList.get(i).equals(reimbList.get(i))) {
				//do nothing
			} else {
				check = false;
			}
		}
		assertFalse(check);
	}
	
	@Test
	public void getReimbursementByAuthorAndStatusFailure() {
		List<ReimbursementEnhanced> tempList = mockedDao.getReimbursementByAuthorAndStatus(1,1);
		boolean check = true;
		if (tempList.isEmpty()) {
			check = false;
		}
		for (int i=0; i<tempList.size(); i++) {
			if (tempList.get(i).equals(reimbList.get(i))) {
				//do nothing
			} else {
				check = false;
			}
		}
		assertFalse(check);
	}
	
	@Test
	public void getReceiptFailure() {
		assertEquals(testService.getReceipt(50), null);
	}
	
	@Test
	public void addReimbursementFailure() {
		System.out.println(timeStamp);
		assertFalse(testService.addReimbursement(150.0, "Socks", 1, 1, 1));
	}
	
	@Test
	public void getReimbursementFailure() {
		assertEquals(testService.getReimbursement(50), null);
	}
	
	@Test
	public void updateReimbursementFailure() {
		assertFalse(testService.updateReimbursement(50, 1, 2, "Comment"));
	}
	
	@Test
	public void addReceiptFailure() {
		assertFalse(testService.addReceipt(50, null));
	}
}
