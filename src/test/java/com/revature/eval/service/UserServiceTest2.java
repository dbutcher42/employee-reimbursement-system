package com.revature.eval.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.revature.dao.UserDao;
import com.revature.model.User;
import com.revature.service.PasswordEncryption;
import com.revature.service.UserService;

public class UserServiceTest2 {
	
	@Mock
	private UserDao mockedDao;
	
	@Mock
	private PasswordEncryption pe; 
	
	@Mock
	private User tempUser;
	
	private UserService testService = new UserService(mockedDao, pe, tempUser);
	private User testUser;
	
	@BeforeClass
	public static void setupBeforeClass() throws Exception {
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}
	
	@Before
	public void setup() throws Exception {
		MockitoAnnotations.initMocks(this);
		testService = new UserService(mockedDao, pe, tempUser);
		testUser = new User(50, "MyUserName", "MyPassword", "MyFirstName", "MyLastName", "MyEmail", 1);
		when(tempUser.generatePassword()).thenReturn("password");
		when(pe.encryptPassword(anyString())).thenReturn("vSQtpGJf4Moqh/j75vXKvjOOp30YpT7C");
		when(pe.decryptPassword(anyString())).thenReturn("password");
		
		when(mockedDao.insertUser("MyUserName", "vSQtpGJf4Moqh/j75vXKvjOOp30YpT7C", "MyFirstName", "MyLastName", "myemail", 1)).thenReturn(false);
		when(mockedDao.getUser(50)).thenReturn(null);
		when(mockedDao.getUserByUserName("MyUserName")).thenReturn(null);
		when(mockedDao.updateUserProfile(50, "NewFirst", "NewLast", "NewEmail", 2)).thenReturn(false);
		when(mockedDao.updateUserPassword(50, "MyNewPassword")).thenReturn(false);
		when(mockedDao.getUserName("MyUserName")).thenReturn("");
		when(mockedDao.getEmail("MyEmail")).thenReturn("");
		when(mockedDao.getPassword("MyUserName")).thenReturn("vSQtpGJf4Moqh/j75vXKvjOOp30YpT7C");
		when(mockedDao.getUserByUserName("Controller1")).thenReturn(null);
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void addUserFailure() {
		assertFalse(testService.addUser("MyUserName", "MyFirstName", "MyLastName", "MyEmail", 1));
	}
	
	@Test
	public void getUserFailure() {
		assertEquals(testService.getUser(50), null);
	}
	
	@Test
	public void getUserByUserNameFailure() {
		assertEquals(testService.getUserByUserName("MyUserName"), null);
	}
	
	@Test
	public void changeUserProfileFailure() {
		assertFalse(testService.changeUserProfile(50, "NewFirst", "NewLast", "NewEmail", 2, "oldemail"));
	}
	
	@Test
	public void changeUserPasswordFailure() {
		assertFalse(testService.changeUserPassword(50, "MyNewPassword"));
	}
	
	@Test
	public void checkUserNameFailure() {
		assertFalse(testService.checkUserName("MyUserName"));
	}
	
	@Test
	public void checkEmailFailure() {
		assertFalse(testService.checkEmail("MyEmail"));
	}
	
	@Test
	public void checkPasswordFailure() {
		assertFalse(testService.checkPassword("MyUserName", "gibberish"));
	}
	
	@Test
	public void retrievePasswordFailure() {
		assertFalse(testService.retrievePassword("Controller1"));
	}
}
