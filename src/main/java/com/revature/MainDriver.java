package com.revature;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.revature.controller.JavalinInitialization;
import com.revature.page.ReimbursementManagementsPage;
import com.revature.page.ReimbursementsPage;

public class MainDriver {

	public final static Logger log = Logger.getLogger(MainDriver.class);
	
	public static void main(String[] args) throws InterruptedException {
		JavalinInitialization.JavalinInt();
		log.setLevel(Level.TRACE);
		
		File file = new File("src/main/resources/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		
		
		
	}

}
