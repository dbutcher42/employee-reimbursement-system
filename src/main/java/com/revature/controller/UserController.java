package com.revature.controller;

import java.util.Map;

import com.revature.service.UserService;
import com.revature.model.User;

import io.javalin.http.Handler;

public class UserController {
	private UserService us = new UserService();
	
	public UserController() {
		// TODO Auto-generated constructor stub
	}
	
	public UserController(UserService us) {
		super();
		this.us = us;
	}

	//add new user
	public final Handler USERPOST1 = (ctx) -> {
		String ersUserName = ctx.formParam("ersUserName");
		String userFirstName = ctx.formParam("userFirstName");
		String userLastName = ctx.formParam("userLastName");
		String userEmail = ctx.formParam("userEmail");
		int userRoleId = Integer.parseInt(ctx.formParam("userRoleId"));
		boolean success = us.addUser(ersUserName, userFirstName, userLastName, userEmail, userRoleId);
		if (success) {
			ctx.status(201);
		} else {
			ctx.status(450);
		}
	};
	
	//check password
	public final Handler USERPOST2 = (ctx) -> {
		String ersUserName = ctx.formParam("ersUserName");
		String ersPassword = ctx.formParam("ersPassword");
		Boolean matches = us.checkPassword(ersUserName, ersPassword);
		if (matches) {
			ctx.status(200);
			ctx.redirect("/html/home.html");
		} else {
			ctx.status(450);
		}
	};
	
	//get user by id
	public final Handler USERGET1 = (ctx) -> {
		int ersUserId = Integer.parseInt(ctx.pathParam("id"));
		User tempUser = us.getUser(ersUserId);
		if (tempUser != null) {
			ctx.status(200);
			ctx.json(new User(tempUser.getErsUserId(), tempUser.getErsUserName(), tempUser.getErsPassword(), tempUser.getUserFirstName(), 
					tempUser.getUserLastName(), tempUser.getUserEmail(), tempUser.getUserRoleId()));
		} else {
			ctx.status(404);
		}
	};
	
	//get user by username
	public final Handler USERGET2 = (ctx) -> {
		String ersUserName = ctx.pathParam("id");
		User tempUser = us.getUserByUserName(ersUserName);
		if (tempUser != null) {
			ctx.json(tempUser);
			ctx.status(200);
		} else {
			ctx.status(404);
		}
	};
	
	//check username
	public final Handler USERGET3 = (ctx) -> {
		String ersUserName = ctx.pathParam("id");
		Boolean isUsed = us.checkUserName(ersUserName);
		if (isUsed) {
			ctx.status(250);
		} else {
			ctx.status(251);
		}
	};
	
	//check email
	public final Handler USERGET4 = (ctx) -> {
		String userEmail = ctx.pathParam("id");
		Boolean isUsed = us.checkEmail(userEmail);
		if (isUsed) {
			ctx.status(250);
		} else {
			ctx.status(251);
		}
	};
	
	//retrieve password
	public final Handler USERGET5 = (ctx) -> {
		String ersUserName = ctx.pathParam("id");
		Boolean found = us.retrievePassword(ersUserName);
		if (found) {
			ctx.status(250);
		} else {
			ctx.status(251);
		}
	};
	
	//update user password
		public final Handler USERPATCH1 = (ctx) -> {
			int ersUserId = Integer.parseInt(ctx.pathParam("id"));
			String ersPassword = ctx.formParam("ersPassword");
			boolean success = us.changeUserPassword(ersUserId, ersPassword);
			if (success) {
				ctx.status(201);
			} else {
				ctx.status(400);
			}
		};
	
	//update user profile
	public final Handler USERPATCH2 = (ctx) -> {
		int ersUserId = Integer.parseInt(ctx.pathParam("id"));
		String userFirstName = ctx.formParam("userFirstName");
		String userLastName = ctx.formParam("userLastName");
		String userEmail = ctx.formParam("userEmail");
		String oldEmail = ctx.formParam("oldEmail");
		int userRoleId = Integer.parseInt(ctx.formParam("userRoleId"));
		boolean success = us.changeUserProfile(ersUserId, userFirstName, userLastName, userEmail, userRoleId, oldEmail);
		if (success) {
			ctx.status(201);
		} else {
			ctx.status(400);
		}
	};
}
