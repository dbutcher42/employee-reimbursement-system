package com.revature.controller;

import java.sql.Blob;
import java.util.List;
import java.util.Map;

import javax.sql.rowset.serial.SerialBlob;

import com.revature.model.Reimbursement;
import com.revature.model.ReimbursementEnhanced;
import com.revature.service.ReimbursementService;

import io.javalin.http.Handler;

public class ReimbursementController {
	private ReimbursementService rs = new ReimbursementService();
	
	public ReimbursementController() {
	}
	
	public ReimbursementController(ReimbursementService rs) {
		super();
		this.rs = rs;
	}

	//get list of reimbursements
	public final Handler REIMBGET1 = (ctx) -> {
		List<ReimbursementEnhanced> reimbList;
		int reimbAuthor = Integer.parseInt(ctx.pathParam("id"));
		reimbList = rs.getReimbursementAll(reimbAuthor);
		ctx.status(200);
		ctx.json(reimbList);
	};
	
	public final Handler REIMBGET2 = (ctx) -> {
		List<ReimbursementEnhanced> reimbList;
		int reimbStatusId = Integer.parseInt(ctx.pathParam("stat"));
		int reimbAuthor = Integer.parseInt(ctx.pathParam("id"));
		reimbList = rs.getReimbursementByStatus(reimbStatusId, reimbAuthor);
		ctx.status(200);
		ctx.json(reimbList);
	};
	
	//get reimbursement by id
	public final Handler REIMBGET3 = (ctx) -> {
		int reimbId = Integer.parseInt(ctx.pathParam("reimb"));
		ReimbursementEnhanced tempR = rs.getReimbursement(reimbId);
		if (tempR != null) {
			ctx.status(200);
			ctx.json(tempR);
		} else {
			ctx.status(404);
		}
	};
	
	//get receipt by id
	public final Handler REIMBGET4 = (ctx) -> {
		int reimbId = Integer.parseInt(ctx.pathParam("reimb"));
		Blob tempReceipt = rs.getReceipt(reimbId);
		if (tempReceipt != null) {
			byte [] bytesReceipt = tempReceipt.getBytes(1l, (int)tempReceipt.length());
			ctx.status(200);
			ctx.result(bytesReceipt);
		} else {
			ctx.status(404);
		}
	};

	public final Handler REIMBGET5 = (ctx) -> {
		List<ReimbursementEnhanced> reimbList;
		int reimbAuthor = Integer.parseInt(ctx.pathParam("id"));
		reimbList = rs.getReimbursementByAuthor(reimbAuthor);
		ctx.status(200);
		ctx.json(reimbList);
	};
	
	public final Handler REIMBGET6 = (ctx) -> {
		List<ReimbursementEnhanced> reimbList;
		int reimbAuthor = Integer.parseInt(ctx.pathParam("id"));
		int reimbStatusId = Integer.parseInt(ctx.pathParam("stat"));
		reimbList = rs.getReimbursementByAuthorAndStatus(reimbAuthor, reimbStatusId);
		ctx.status(200);
		ctx.json(reimbList);
	};
	
	public final Handler REIMBPOST = (ctx) -> {
		Double reimbAmount = Double.parseDouble(ctx.formParam("reimbAmount"));
		String reimbDescription = ctx.formParam("reimbDescription");
		int reimbAuthor = Integer.parseInt(ctx.formParam("reimbAuthor"));
		int reimbStatusId = 1;
		int reimbTypeId = Integer.parseInt(ctx.formParam("reimbTypeId"));
		boolean success = rs.addReimbursement(reimbAmount, reimbDescription, reimbAuthor, reimbStatusId, reimbTypeId);
		if (success) {
			ctx.status(201);
		} else {
			ctx.status(400);
		}
	};
	
	public final Handler REIMBPATCH1 = (ctx) -> {
		int reimbId = Integer.parseInt(ctx.pathParam("reimb"));
		byte[] tempReceipt = ctx.bodyAsBytes();
		Blob reimbReceipt = new SerialBlob(tempReceipt);
		boolean success = rs.addReceipt(reimbId, reimbReceipt);
		if (success) {
			ctx.status(201);
		} else {
			ctx.status(404);
		}
	};
	
	public final Handler REIMBPATCH2 = (ctx) -> {
		int reimbId = Integer.parseInt(ctx.pathParam("reimb"));
		int reimbResolver = Integer.parseInt(ctx.formParam("reimbResolver"));
		int reimbStatusId = Integer.parseInt(ctx.formParam("reimbStatusId"));
		String reimbComment = ctx.formParam("reimbComment");
		boolean success = rs.updateReimbursement(reimbId, reimbResolver, reimbStatusId, reimbComment);
		if (success) {
			ctx.status(201);
		} else {
			ctx.status(400);
		}
	};
}
