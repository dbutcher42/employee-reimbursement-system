package com.revature.controller;

import com.revature.MainDriver;
import com.revature.dao.DaoConnection;
import com.revature.dao.ReimbursementDao;
import com.revature.dao.UserDao;
import com.revature.model.User;
import com.revature.service.PasswordEncryption;
import com.revature.service.ReimbursementService;
import com.revature.service.UserService;

import io.javalin.Javalin;

public class JavalinInitialization {
	public static void JavalinInt() {
		User tempUser = new User();
		PasswordEncryption pe = new PasswordEncryption();
		UserController uc = new UserController(new UserService(new UserDao(new DaoConnection()), pe, tempUser));
		ReimbursementController rec = new ReimbursementController(new ReimbursementService(new ReimbursementDao(new DaoConnection())));
		
		Javalin app = Javalin.create(config -> {
			config.addStaticFiles("/frontend");
			config.enableCorsForAllOrigins();
		});
		
		app.start(9001);

		app.post("/users", uc.USERPOST1);												//add user
		app.post("/users/passwords", uc.USERPOST2);										//check pw
		app.get("/users/:id", uc.USERGET1);												//get specific user
		app.get("/users/namedusers/:id", uc.USERGET2);									//get user by username
		app.get("/users/usernames/:id", uc.USERGET3);									//check user name
		app.get("/users/emails/:id", uc.USERGET4);										//check email
		app.get("/users/:id/passwords", uc.USERGET5);									//retrieve password
		app.patch("/users/:id/passwords", uc.USERPATCH1);								//update password
		app.patch("/users/:id/profiles", uc.USERPATCH2);								//update profile

		app.get("/users/:id/managedreimbursements", rec.REIMBGET1);						//get reimb lists - omits user
		app.get("/users/:id/managedreimbursements/statuses/:stat", rec.REIMBGET2);		//get reimb lists for specific status - omits user
		app.get("/users/:id/reimbursements/:reimb", rec.REIMBGET3);						//get specific reimbursement
		app.get("/users/:id/reimbursements/:reimb/receipts", rec.REIMBGET4);			//get receipt
		app.get("/users/:id/reimbursements", rec.REIMBGET5);							//get reimb lists for specific user
		app.get("/users/:id/reimbursements/statuses/:stat", rec.REIMBGET6);				//get reimb lists for specific user for specific status
		app.post("/users/:id/reimbursements", rec.REIMBPOST);							//add reimbursement
		app.patch("/users/:id/reimbursements/:reimb/receipts", rec.REIMBPATCH1);		//add reciept to reimbursement
		app.patch("/users/:id/reimbursements/:reimb", rec.REIMBPATCH2);					//update reimbursement
		
		app.exception(NullPointerException.class, (e, ctx) -> {
			e.printStackTrace();
			ctx.status(404);
		}).error(404, (ctx)->{
			ctx.result("404 NullPointerException");
		});
		
		MainDriver.log.info("Javalin Initialization Complete");

	}
}
