package com.revature.model;

import java.sql.Blob;
import java.sql.Timestamp;
import java.util.Arrays;

public class ReimbursementEnhanced extends Reimbursement	{
	private String authorName;
	private String resolverName;
	private String typeName;
	private String statusName;

	
	public ReimbursementEnhanced() {
	}

	public ReimbursementEnhanced(int reimbId, double reimbAmount, Timestamp reimbSubmitted, Timestamp reimbResolved,
			String reimbDescription, Blob reimbReceipt, int reimbAuthor, int reimbResolver, int reimbStatusId,
			int reimbTypeId, String reimbComment, String authorName, String resolverName, String typeName,
			String statusName) {
		super(reimbId, reimbAmount, reimbSubmitted, reimbResolved,
				reimbDescription, reimbReceipt, reimbAuthor, reimbResolver, reimbStatusId,
				reimbTypeId, reimbComment);
		this.authorName = authorName;
		this.resolverName = resolverName;
		this.statusName = statusName;
		this.typeName = typeName;
	}

	public ReimbursementEnhanced(double reimbAmount, Timestamp reimbSubmitted, Timestamp reimbResolved,
			String reimbDescription, Blob reimbReceipt, int reimbAuthor, int reimbResolver, int reimbStatusId,
			int reimbTypeId, String reimbComment, String authorName, String resolverName, String typeName,
			String statusName) {
		super(reimbAmount, reimbSubmitted, reimbResolved,
				reimbDescription, reimbReceipt, reimbAuthor, reimbResolver, reimbStatusId,
				reimbTypeId, reimbComment);
		this.authorName = authorName;
		this.resolverName = resolverName;
		this.statusName = statusName;
		this.typeName = typeName;
	}
	
	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public String getResolverName() {
		return resolverName;
	}

	public void setResolverName(String resolverName) {
		this.resolverName = resolverName;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	@Override
	public String toString() {
		return "ReimbursementEnhanced [reimbId=" + getReimbId() + ", reimbAmount=" + getReimbAmount() + ", reimbSubmitted="
				+ getReimbSubmitted() + ", reimbResolved=" + getReimbResolved() + ", reimbDescription=" + getReimbDescription()
				+ ", reimbAuthor=" + getReimbAuthor() + ", reimbResolver=" + getReimbResolver() + ", reimbStatusId="
				+ getReimbStatusId() + ", reimbTypeId=" + getReimbTypeId() + ", reimbComment=" + getReimbComment() + ", authorName="
				+ authorName + ", resolverName=" + resolverName + ", statusName=" + statusName + ", typeName="
				+ typeName + "]";
	}

	public boolean equals(ReimbursementEnhanced or) {
		if (this.getReimbId() == or.getReimbId() && this.getReimbAmount() == or.getReimbAmount() && this.getReimbSubmitted() == or.getReimbSubmitted() &&
				this.getReimbResolved() == or.getReimbResolved() && this.getReimbDescription().equals(or.getReimbDescription()) && 
				this.getReimbReceipt() == or.getReimbReceipt() && this.getReimbAuthor() == or.getReimbAuthor() && this.getReimbResolver() == or.getReimbResolver() &&
				this.getReimbStatusId() == or.getReimbStatusId() && this.getReimbTypeId() == or.getReimbTypeId() && this.getReimbComment().equals(or.getReimbComment())) {
			return true;
		} else {
			return false;
		}
	}
}
