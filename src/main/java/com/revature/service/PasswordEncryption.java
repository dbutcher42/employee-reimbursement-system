package com.revature.service;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

public class PasswordEncryption {
	private static final String SEED = "mySecretSeed";
	
	public String encryptPassword(String passWordIn) {
		StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
		encryptor.setPassword(SEED);
		String encryptedPassword = encryptor.encrypt(passWordIn);
		return encryptedPassword;
	}
	
	public String decryptPassword(String passWordIn) {
		StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
		encryptor.setPassword(SEED);
		String decryptedPassword = encryptor.decrypt(passWordIn);
		return decryptedPassword;
	}
}
