package com.revature.service;

import com.revature.MainDriver;
import com.revature.dao.UserDao;
import com.revature.model.User;

public class UserService {
	private UserDao userDao = new UserDao();
	private PasswordEncryption pe;
	private User tempUser;
	
	public UserService() {

	}

	public UserService(UserDao userDao, PasswordEncryption pe, User tempUser) {
		super();
		this.userDao = userDao;
		this.pe = pe;
		this.tempUser = tempUser;
	}

	public User getUser(int ersUserId) {
		User tempUser = userDao.getUser(ersUserId);
		return tempUser;
	}
	
	public User getUserByUserName(String ersUserName) {
		User tempUser = userDao.getUserByUserName(ersUserName);
		return tempUser;
	}

	public boolean addUser(String ersUserName, String userFirstName, String userLastName, String userEmail, int userRoleId) {
		String tempEmail = userEmail.toLowerCase();
		String tempPassword = tempUser.generatePassword();
		String encryptedPassword = pe.encryptPassword(tempPassword);
		EmailService.sendRegistrationEmail(ersUserName, tempPassword, userFirstName, userLastName, userEmail);
		MainDriver.log.info("Password email sent for ersUserName: " + ersUserName);
		MainDriver.log.info("New user added for ersUserName: " + ersUserName);
		return userDao.insertUser(ersUserName, encryptedPassword, userFirstName, userLastName, tempEmail, userRoleId);
	}
	
	public boolean changeUserProfile(int ersUserId, String userFirstName, String userLastName, String userEmail, int userRoleId, String oldEmail) {
		User tempUser = new User();
		String tempEmail = userEmail.toLowerCase();
		if (oldEmail.equals(tempEmail)) {
		} else {
			if (checkEmail(tempEmail)) {
				MainDriver.log.info("Duplicate Email: User profile update FAILED for ersUserId: " + ersUserId);
				return false;
			}
		}
		
		MainDriver.log.info("User profile updated for ersUserId: " + ersUserId);
		return userDao.updateUserProfile(ersUserId, userFirstName, userLastName, tempEmail, userRoleId);
	}
	
	public boolean changeUserPassword(int ersUserId, String ersPassword) {
		String encryptedPassword = pe.encryptPassword(ersPassword);
		MainDriver.log.info("User password updated for ersUserId: " + ersUserId);
		return userDao.updateUserPassword(ersUserId, encryptedPassword);
	}
	
	public Boolean checkUserName(String ersUserName) {
		String tempUserName = userDao.getUserName(ersUserName); 
		if (tempUserName == null) {
			return false;
		} else {
			if (tempUserName.equals(ersUserName)) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	public Boolean checkEmail(String userEmail) {
		String tempEmail = userDao.getEmail(userEmail);
		String userEmailLower = userEmail.toLowerCase();
		if (tempEmail == null) {
			return false;
		} else {
			if (tempEmail.equals(userEmailLower)) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	public Boolean checkPassword(String ersUserName, String ersPassword) {
		String decryptedPassword;
		String tempPassword = userDao.getPassword(ersUserName);
		PasswordEncryption pe = new PasswordEncryption();
		decryptedPassword = pe.decryptPassword(tempPassword);
		if (decryptedPassword == null) {
			MainDriver.log.info("Login FAILED for ersUserName: " + ersUserName);
			return false;
		}
		if (ersPassword.equals(decryptedPassword)) {
			MainDriver.log.info("Login succeeded for ersUserName: " + ersUserName);
			return true;
		} else {
			MainDriver.log.info("Login FAILED for ersUserName: " + ersUserName);
			return false;
		}
	}
	
	public Boolean retrievePassword(String ersUserName) {
		String tempPassword = userDao.getPassword(ersUserName);
		PasswordEncryption pe = new PasswordEncryption();
		String decryptedPassword = pe.decryptPassword(tempPassword);
		if (decryptedPassword == null) {
			MainDriver.log.info("Password retrieval FAILED for ersUserName: " + ersUserName);
			return false;
		}
		User tempUser = userDao.getUserByUserName(ersUserName);
		EmailService.sendPasswordEmail(ersUserName, decryptedPassword, tempUser.getUserFirstName(), tempUser.getUserLastName(), tempUser.getUserEmail());
		MainDriver.log.info("Password retrieval successful for ersUserName: " + ersUserName);
		return true;
	}
}
