package com.revature.service;

import java.util.*; 
import javax.mail.*; 
import javax.mail.internet.*;

import com.revature.MainDriver;

import javax.activation.*; 
import javax.mail.Session; 
import javax.mail.Transport; 

public class EmailService {
	public static void sendRegistrationEmail(String ersUserName, String ersPassword, String userFirstName, String userLastName, String userEmail) {
        final String username = "projectcafeflora@gmail.com";
        final String password = "Newtoast2#";

        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
          new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
          });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("danbutcher42@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse("danbutcher42@gmail.com"));
            message.setSubject("Expense Reimbursement System User Registration Confirmation");
            message.setText("Dear " + userFirstName + " " + userLastName + ","
                + "\n\n Your registration for the Expense Reimbursement System is complete. "
                + "\n\n Your temporary password for user name " + ersUserName + " is " + ersPassword + ".  Please change your password immediately.");

            Transport.send(message);

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
	}
	
	public static void sendPasswordEmail(String ersUserName, String ersPassword, String userFirstName, String userLastName, String userEmail) {
        final String username = "projectcafeflora@gmail.com";
        final String password = "Newtoast2#";

        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
          new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
          });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("danbutcher42@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse("danbutcher42@gmail.com"));
            message.setSubject("Expense Reimbursement System User Password");
            message.setText("Dear " + userFirstName + " " + userLastName + ","
                + "\n\n Your password for user name " + ersUserName + " is " + ersPassword + ".");

            Transport.send(message);
            
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
	}
	
}
