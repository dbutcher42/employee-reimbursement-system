package com.revature.service;

import java.sql.Blob;
import java.util.List;

import com.revature.MainDriver;
import com.revature.dao.ReimbursementDao;
import com.revature.model.Reimbursement;
import com.revature.model.ReimbursementEnhanced;

public class ReimbursementService {
	private ReimbursementDao reimbDao = new ReimbursementDao();
	
	public ReimbursementService() {

	}

	public ReimbursementService(ReimbursementDao reimbDao) {
		super();
		this.reimbDao = reimbDao;
	}
	
	public List<ReimbursementEnhanced> getReimbursementAll(int reimbAuthor) {
		List<ReimbursementEnhanced> reimbList = reimbDao.getReimbursementAll(reimbAuthor);
		return reimbList;
	}
	
	public List<ReimbursementEnhanced> getReimbursementByStatus(int reimbStatusId, int reimbAuthor) {
		List<ReimbursementEnhanced> reimbList = reimbDao.getReimbursementByStatus(reimbStatusId, reimbAuthor);
		return reimbList;
	}
	
	public List<ReimbursementEnhanced> getReimbursementByAuthor(int reimbAuthor) {
		List<ReimbursementEnhanced> reimbList = reimbDao.getReimbursementByAuthor(reimbAuthor);
		return reimbList;
	}
	
	public List<ReimbursementEnhanced> getReimbursementByAuthorAndStatus(int reimbAuthor, int reimbStatusId) {
		List<ReimbursementEnhanced> reimbList = reimbDao.getReimbursementByAuthorAndStatus(reimbAuthor, reimbStatusId);
		return reimbList;
	}
	
	public ReimbursementEnhanced getReimbursement(int reimbId) {
		ReimbursementEnhanced tempReimb = reimbDao.getReimbursement(reimbId);
		return tempReimb;
	}
	
	public Blob getReceipt(int reimbId) {
		Blob tempReceipt = reimbDao.getReceipt(reimbId);
		return tempReceipt;
	}
	
	public boolean addReimbursement(double reimbAmount, String reimbDescription, int reimbAuthor, int reimbStatusId, int reimbTypeId) {
		MainDriver.log.info("Reimbursement added for reimbAuthor: " + reimbAuthor);
		
		return reimbDao.insertReimbursement(reimbAmount, reimbDescription, reimbAuthor, reimbStatusId, reimbTypeId);
	}
	
	public boolean addReceipt(int reimbId, Blob reimbReceipt) {
		MainDriver.log.info("Receipt added for reimbId: " + reimbId);
		return reimbDao.updateReceipt(reimbId, reimbReceipt);
	}
	
	public boolean updateReimbursement(int reimbId, int reimbResolver, int reimbStatusId, String reimb_comment) {
		MainDriver.log.info("Reimbursement reimbId: " + reimbId + " updated by reimbResolver: " + reimbResolver);
		return reimbDao.updateReimbursement(reimbId, reimbResolver, reimbStatusId, reimb_comment);
	}
	
}
