package com.revature.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DaoConnection {
	private static String url = "jdbc:mariadb://database-1.czar35oclblc.us-east-2.rds.amazonaws.com:3306/project1db";
	private static String userName = "firstuser";
	private static String password = "mypassword";
	
	public Connection getDBConnection() throws SQLException {
		return DriverManager.getConnection(url, userName, password);
	}
}
