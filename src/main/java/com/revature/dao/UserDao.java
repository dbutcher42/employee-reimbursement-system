package com.revature.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.revature.MainDriver;
import com.revature.model.User;
import com.revature.service.EmailService;
import com.revature.service.PasswordEncryption;

public class UserDao {
	
	private DaoConnection dc;
	
	public UserDao() {
		dc = new DaoConnection();
	}

	public UserDao(DaoConnection dc) {
		super();
		this.dc = dc;
	}
	
	//GET USER
	public User getUser(int ersUserId) {
		User foundUser = new User();
		
		try (Connection conn = dc.getDBConnection()) {
			String sql = "{call select_user(?)}";
			CallableStatement cs = conn.prepareCall(sql);
			cs.setInt(1, ersUserId);
			ResultSet rs = cs.executeQuery();
			rs.next();
			foundUser = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7));
		}catch(SQLException e) {
			foundUser = null;
			MainDriver.log.warn("SQL Exception encountered in getUser");
			MainDriver.log.warn(e.getMessage());
			e.printStackTrace();
		}catch(Exception e) {
			foundUser = null;
			MainDriver.log.warn("Exception encountered in getUser");
			MainDriver.log.warn(e.getMessage());
			e.printStackTrace();
		}
		return foundUser;
	}
	
	//GET USER BY USERNAME
	public User getUserByUserName(String ersUserName) {
		User foundUser = new User();
		
		try (Connection conn = dc.getDBConnection()) {
			String sql = "{call select_user_by_user_name(?)}";
			CallableStatement cs = conn.prepareCall(sql);
			cs.setString(1, ersUserName);
			ResultSet rs = cs.executeQuery();
			rs.next();
			foundUser = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7));
		}catch(SQLException e) {
			foundUser = null;
			MainDriver.log.warn("SQL Exception encountered in getUserByUserName");
			MainDriver.log.warn(e.getMessage());
			e.printStackTrace();
		}catch(Exception e) {
			foundUser = null;
			MainDriver.log.warn("Exception encountered in getUserByUserName");
			MainDriver.log.warn(e.getMessage());
			e.printStackTrace();
		}
		return foundUser;
	}
	
	//INSERT NEW USER
	public boolean insertUser(String ersUserName, String ersPassword, String userFirstName, String userLastName, String userEmail, int userRoleId) {
		int changed = 0;
		
		try (Connection conn = dc.getDBConnection()) {
			String sql = "{call insert_user(?,?,?,?,?,?)}";
			CallableStatement cs = conn.prepareCall(sql);

			cs.setString(1, ersUserName);
			cs.setString(2, ersPassword);
			cs.setString(3, userFirstName);
			cs.setString(4, userLastName);
			cs.setString(5, userEmail);
			cs.setInt(6,  userRoleId);
			changed = cs.executeUpdate();
//			System.out.println("Changed Rows: " + changed);
		}catch(SQLException e) {
			MainDriver.log.warn("SQL Exception encountered in insertUser");
			MainDriver.log.warn(e.getMessage());
			//e.printStackTrace();
		}catch(Exception e) {
			MainDriver.log.warn("Exception encountered in insertUser");
			MainDriver.log.warn(e.getMessage());
			e.printStackTrace();
		}
		if (changed==1) {
			return true;
		} else {
			return false;
		}
	}
	
	//UPDATE USER NOT PASSWORD OR USER ID
	public boolean updateUserProfile(int ersUserId, String userFirstName, String userLastName, String userEmail, int userRoleId) {
		int changed = 0;
		
		try (Connection conn = dc.getDBConnection()) {
			String sql = "{call update_user_not_id_or_password(?,?,?,?,?)}";
			CallableStatement cs = conn.prepareCall(sql);
			cs.setInt(1, ersUserId);
			cs.setString(2, userFirstName);
			cs.setString(3, userLastName);
			cs.setString(4, userEmail);
			cs.setInt(5,  userRoleId);
			changed = cs.executeUpdate();
//			System.out.println("Changed Rows: " + changed);
		}catch(SQLException e) {
			MainDriver.log.warn("SQL Exception encountered in updateUserProfile");
			MainDriver.log.warn(e.getMessage());
			e.printStackTrace();
		}catch(Exception e) {
			MainDriver.log.warn("Exception encountered in updateUserProfile");
			MainDriver.log.warn(e.getMessage());
			e.printStackTrace();
		}
		if (changed==1) {
			return true;
		} else {
			return false;
		}
	}
	
	//UPDATE USER PASSWORD ONLY
	public boolean updateUserPassword(int ersUserId, String ersPassword) {
		int changed = 0;
		
		try (Connection conn = dc.getDBConnection()) {
			String sql = "{call update_user_password_only(?,?)}";
			CallableStatement cs = conn.prepareCall(sql);
			cs.setInt(1, ersUserId);
			cs.setString(2, ersPassword);
			changed = cs.executeUpdate();
//			System.out.println("Changed Rows: " + changed);
		}catch(SQLException e) {
			MainDriver.log.warn("SQL Exception encountered in updateUserPassword");
			MainDriver.log.warn(e.getMessage());
		}catch(Exception e) {
			MainDriver.log.warn("Exception encountered in updateUserPassword");
			MainDriver.log.warn(e.getMessage());
			e.printStackTrace();
		}
		if (changed==1) {
			return true;
		} else {
			return false;
		}
	}

	//GET USER NAME TO CHECK UNIQUENESS
	public String getUserName(String ersUserName) {
		String foundUserName = "";
		
		try (Connection conn = dc.getDBConnection()) {
			String sql = "{call find_user_name(?)}";
			CallableStatement cs = conn.prepareCall(sql);
			cs.setString(1, ersUserName);
			ResultSet rs = cs.executeQuery();
			rs.next();
			foundUserName = rs.getString(1);
		}catch(SQLException e) {
			foundUserName = null;
		}catch(Exception e) {
			foundUserName = null;
			MainDriver.log.warn("Exception encountered in getUserName");
			MainDriver.log.warn(e.getMessage());
			e.printStackTrace();
		}
		return foundUserName;
	}
	
	//GET USER EMAIL TO CHECK UNIQUENESS
	public String getEmail(String userEmail) {
		String foundEmail = "";
		
		try (Connection conn = dc.getDBConnection()) {
			String sql = "{call find_email(?)}";
			CallableStatement cs = conn.prepareCall(sql);
			cs.setString(1, userEmail);
			ResultSet rs = cs.executeQuery();
			rs.next();
			foundEmail = rs.getString(1);
		}catch(SQLException e) {
			foundEmail = null;
		}catch(Exception e) {
			foundEmail = null;
			MainDriver.log.warn("Exception encountered in getUserName");
			MainDriver.log.warn(e.getMessage());
			e.printStackTrace();
		}
		return foundEmail;
	}
	
	//GET PASSWORD
	public String getPassword(String ersUserName) {
		String foundPassword = "";
		try (Connection conn = dc.getDBConnection()) {
			String sql = "{call find_credentials(?)}";
			CallableStatement cs = conn.prepareCall(sql);
			cs.setString(1, ersUserName);
			ResultSet rs = cs.executeQuery();
			rs.next();
			foundPassword = rs.getString(1);
		}catch(SQLException e) {
			foundPassword = null;
		}catch(Exception e) {
			foundPassword = null;
			MainDriver.log.warn("Exception encountered in getUserName");
			MainDriver.log.warn(e.getMessage());
			e.printStackTrace();
		}
		return foundPassword;
	}

}
