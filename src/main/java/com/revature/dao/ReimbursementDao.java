package com.revature.dao;

import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.revature.MainDriver;
import com.revature.model.Reimbursement;
import com.revature.model.ReimbursementEnhanced;
import com.revature.model.User;

public class ReimbursementDao {
	
	private DaoConnection dc;
	
	public ReimbursementDao() {
		dc = new DaoConnection();
	}

	public ReimbursementDao(DaoConnection dc) {
		super();
		this.dc = dc;
	}
	
	//GET REIMBURSEMENT LIST
	public List<ReimbursementEnhanced> getReimbursementAll(int reimbAuthor) {
		List<ReimbursementEnhanced> reimbList = new ArrayList<ReimbursementEnhanced>();
		
		try (Connection conn = dc.getDBConnection()) {
			String sql = "{call select_reimbursement_all(?)}";
			CallableStatement cs = conn.prepareCall(sql);
			cs.setInt(1, reimbAuthor);
			ResultSet rs = cs.executeQuery();
			while(rs.next()) {
				reimbList.add(new ReimbursementEnhanced(rs.getInt(1), rs.getDouble(2), rs.getTimestamp(3), rs.getTimestamp(4), 
						rs.getString(5), null, rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getString(11), 
						rs.getString(12), rs.getString(13), rs.getString(14), rs.getString(15)));
			}
		}catch(SQLException e) {
			MainDriver.log.warn("SQL Exception encountered in getReimbursementAll");
			MainDriver.log.warn(e.getMessage());
			e.printStackTrace();
		}catch(Exception e) {
			MainDriver.log.warn("Exception encountered in getReimbursementAll");
			MainDriver.log.warn(e.getMessage());
			e.printStackTrace();
		}
			
		return reimbList;
	}
	
	//GET REIMBURSEMENT LIST BY STATUS
	public List<ReimbursementEnhanced> getReimbursementByStatus(int reimbStatusId, int reimbAuthor) {
		List<ReimbursementEnhanced> reimbList = new ArrayList<ReimbursementEnhanced>();
		
		try (Connection conn = dc.getDBConnection()) {
			String sql = "{call select_reimbursement_by_status(?,?)}";
			CallableStatement cs = conn.prepareCall(sql);
			cs.setInt(1, reimbStatusId);
			cs.setInt(2, reimbAuthor);
			ResultSet rs = cs.executeQuery();
			while(rs.next()) {
				reimbList.add(new ReimbursementEnhanced(rs.getInt(1), rs.getDouble(2), rs.getTimestamp(3), rs.getTimestamp(4), 
						rs.getString(5), null, rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getString(11), 
						rs.getString(12), rs.getString(13), rs.getString(14), rs.getString(15)));
			}
		}catch(SQLException e) {
			MainDriver.log.warn("SQL Exception encountered in getReimbursementByStatus");
			MainDriver.log.warn(e.getMessage());
			e.printStackTrace();
		}catch(Exception e) {
			MainDriver.log.warn("Exception encountered in getReimbursementByStatus");
			MainDriver.log.warn(e.getMessage());
			e.printStackTrace();
		}
			
		return reimbList;
	}
	
	//GET REIMBURSEMENT LIST BY AUTHOR
	public List<ReimbursementEnhanced> getReimbursementByAuthor(int reimbAuthor) {
		List<ReimbursementEnhanced> reimbList = new ArrayList<ReimbursementEnhanced>();
		
		try (Connection conn = dc.getDBConnection()) {
			String sql = "{call select_reimbursement_by_author(?)}";
			CallableStatement cs = conn.prepareCall(sql);
			cs.setInt(1, reimbAuthor);
			ResultSet rs = cs.executeQuery();
			while(rs.next()) {
				reimbList.add(new ReimbursementEnhanced(rs.getInt(1), rs.getDouble(2), rs.getTimestamp(3), rs.getTimestamp(4), 
						rs.getString(5), null, rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getString(11), 
						rs.getString(12), rs.getString(13), rs.getString(14), rs.getString(15)));
			}
		}catch(SQLException e) {
			MainDriver.log.warn("SQL Exception encountered in getReimbursementByAuthor");
			MainDriver.log.warn(e.getMessage());
			e.printStackTrace();
		}catch(Exception e) {
			MainDriver.log.warn("Exception encountered in getReimbursementByAuthor");
			MainDriver.log.warn(e.getMessage());
			e.printStackTrace();
		}
			
		return reimbList;
	}
	
	//GET REIMBURSEMENT LIST BY AUTHOR AND STATUS
	public List<ReimbursementEnhanced> getReimbursementByAuthorAndStatus(int reimbAuthor, int reimbStatusId) {
		List<ReimbursementEnhanced> reimbList = new ArrayList<ReimbursementEnhanced>();
		
		try (Connection conn = dc.getDBConnection()) {
			String sql = "{call select_reimbursement_by_author_and_status(?, ?)}";
			CallableStatement cs = conn.prepareCall(sql);
			cs.setInt(1, reimbAuthor);
			cs.setInt(2, reimbStatusId);
			ResultSet rs = cs.executeQuery();
			while(rs.next()) {
				reimbList.add(new ReimbursementEnhanced(rs.getInt(1), rs.getDouble(2), rs.getTimestamp(3), rs.getTimestamp(4), 
						rs.getString(5), null, rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getString(11), 
						rs.getString(12), rs.getString(13), rs.getString(14), rs.getString(15)));
			}
		}catch(SQLException e) {
			MainDriver.log.warn("SQL Exception encountered in getReimbursementByAuthor");
			MainDriver.log.warn(e.getMessage());
			e.printStackTrace();
		}catch(Exception e) {
			MainDriver.log.warn("Exception encountered in getReimbursementByAuthor");
			MainDriver.log.warn(e.getMessage());
			e.printStackTrace();
		}
			
		return reimbList;
	}
	
	//GET REIMBURSEMENT
	public ReimbursementEnhanced getReimbursement(int reimbId) {
		ReimbursementEnhanced foundReimb = new ReimbursementEnhanced();
		
		try (Connection conn = dc.getDBConnection()) {
			String sql = "{call select_reimbursement(?)}";
			CallableStatement cs = conn.prepareCall(sql);
			cs.setInt(1, reimbId);
			ResultSet rs = cs.executeQuery();
			rs.next();
			foundReimb = new ReimbursementEnhanced(rs.getInt(1), rs.getDouble(2), rs.getTimestamp(3), rs.getTimestamp(4), 
					rs.getString(5), null, rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getString(11),
					rs.getString(12), rs.getString(13), rs.getString(14), rs.getString(15));
		}catch(SQLException e) {
			foundReimb = null;
			MainDriver.log.warn("SQL Exception encountered in getReimbursement");
			MainDriver.log.warn(e.getMessage());
			e.printStackTrace();
		}catch(Exception e) {
			MainDriver.log.warn("Exception encountered in getReimbursement");
			MainDriver.log.warn(e.getMessage());
			e.printStackTrace();
		}
			
		return foundReimb;
	}
	
	//GET RECEIPT
	public Blob getReceipt(int reimbId) {
		Blob foundReceipt = null;
		
		try (Connection conn = dc.getDBConnection()) {
			String sql = "{call select_receipt(?)}";
			CallableStatement cs = conn.prepareCall(sql);
			cs.setInt(1, reimbId);
			ResultSet rs = cs.executeQuery();
			rs.next();
			foundReceipt = rs.getBlob(1);
		}catch(SQLException e) {
			MainDriver.log.warn("SQL Exception encountered in getReceipt");
			MainDriver.log.warn(e.getMessage());
			e.printStackTrace();
		}catch(Exception e) {
			MainDriver.log.warn("Exception encountered in getReceipt");
			MainDriver.log.warn(e.getMessage());
			e.printStackTrace();
		}
			
		return foundReceipt;
	}
	
	//INSERT NEW REIMBURSEMENT
	public boolean insertReimbursement(double reimbAmount, String reimbDescription,	int reimbAuthor, int reimbStatusId, int reimbTypeId) {
		int changed = 0;
		
		try (Connection conn = dc.getDBConnection()) {
			String sql = "{call insert_reimbursement(?,?,?,?,?)}";
			CallableStatement cs = conn.prepareCall(sql);
			cs.setDouble(1, reimbAmount);
			cs.setString(2, reimbDescription);
			cs.setInt(3, reimbAuthor);
			cs.setInt(4,  reimbStatusId);
			cs.setInt(5,  reimbTypeId);
			changed = cs.executeUpdate();
//			System.out.println("Changed Rows: " + changed);
	
		}catch(SQLException e) {
			MainDriver.log.warn("SQL Exception encountered in insertReimbursement");
			MainDriver.log.warn(e.getMessage());
			e.printStackTrace();
		}catch(Exception e) {
			MainDriver.log.warn("Exception encountered in insertReimbursement");
			MainDriver.log.warn(e.getMessage());
			e.printStackTrace();
		}
		if (changed==1) {
			return true;
		} else {
			return false;
		}
	}
	
	//UPDATE RECEIPT
	public boolean updateReceipt(int reimbId, Blob reimbReceipt) {
		int changed = 0;
		
		try (Connection conn = dc.getDBConnection()) {
			String sql = "{call update_receipt(?,?)}";
			CallableStatement cs = conn.prepareCall(sql);
			cs.setInt(1, reimbId);
			cs.setBlob(2, reimbReceipt);
			changed = cs.executeUpdate();
//			System.out.println("Changed Rows: " + changed);
		}catch(SQLException e) {
			MainDriver.log.warn("SQL Exception encountered in updateReceipt");
			MainDriver.log.warn(e.getMessage());
			e.printStackTrace();
		}catch(Exception e) {
			MainDriver.log.warn("Exception encountered in updateReceipt");
			MainDriver.log.warn(e.getMessage());
			e.printStackTrace();
		}
		if (changed==1) {
			return true;
		} else {
			return false;
		}
	}
	
	//UPDATE REIMBURSEMENT
	public boolean updateReimbursement(int reimbId, int reimbResolver, int reimbStatusId, String reimb_comment) {
		int changed = 0;
		
		try (Connection conn = dc.getDBConnection()) {
			String sql = "{call update_reimbursement(?,?,?,?)}";
			CallableStatement cs = conn.prepareCall(sql);
			cs.setInt(1, reimbId);
			cs.setInt(2, reimbResolver);
			cs.setInt(3, reimbStatusId);
			cs.setString(4, reimb_comment);
			changed = cs.executeUpdate();
//			System.out.println("Changed Rows: " + changed);
		}catch(SQLException e) {
			MainDriver.log.warn("SQL Exception encountered in updateReimbursement");
			MainDriver.log.warn(e.getMessage());
			e.printStackTrace();
		}catch(Exception e) {
			MainDriver.log.warn("Exception encountered in updateReimbursement");
			MainDriver.log.warn(e.getMessage());
			e.printStackTrace();
		}
		if (changed==1) {
			return true;
		} else {
			return false;
		}
	}
	
}
