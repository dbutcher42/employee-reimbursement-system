package com.revature.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class NewPasswordsPage {
	private WebDriver driver;
	private WebElement homeButton;
	private WebElement manageButton;
	private WebElement userAccountButton;
	private WebElement profilesButton;
	private WebElement passwordsButton;
	private WebElement logoutButton;
	private WebElement updateButton;
	private WebElement cancelButton;
	private WebElement passwordField;

	
	public NewPasswordsPage(WebDriver driver, String ersUserName, int ersUserId) {
		this.driver = driver;
		this.navigateTo(ersUserName, ersUserId);
		
		this.homeButton = driver.findElement(By.id("homelink"));
		this.manageButton = driver.findElement(By.id("managelink"));
		this.userAccountButton = driver.findElement(By.id("navbarDropdownMenuLink"));
		this.profilesButton = driver.findElement(By.id("buttonProfile"));
		this.passwordsButton = driver.findElement(By.id("buttonPassword"));
		this.logoutButton = driver.findElement(By.id("buttonLogout"));
		
		this.updateButton = driver.findElement(By.id("submitbutton"));
		this.cancelButton = driver.findElement(By.id("cancelbutton"));
		this.passwordField = driver.findElement(By.id("ersPassword"));

	}
	
	public void clickHomeButton() {
		this.homeButton.click();
	}
	
	public void clickManageButton() {
		this.manageButton.click();
	}
	
	public void clickProfilesButton() {
		this.userAccountButton.click();
		this.profilesButton.click();
	}
	
	public void clickPasswordsButton() {
		this.userAccountButton.click();
		this.passwordsButton.click();
	}
	
	public void clickLogoutButton() {
		this.userAccountButton.click();
		this.logoutButton.click();
	}
	
	public void clickUpdateButton() {
		this.updateButton.click();
	}
	
	public void clickCancelButton() {
		this.cancelButton.click();
	}
	
	public void setPassword(String ersPassword) {
		this.passwordField.clear();
		this.passwordField.sendKeys(ersPassword);
	}
	
	public String getPassword() {
		return this.passwordField.getAttribute("value");
	}
	
	public void navigateTo(String ersUserName, int ersUserId) {
		this.driver.get("http://localhost:9001/html/newpasswords.html?ersUserId=" + ersUserId + "&ersUsername=" + ersUserName);
	}
}
