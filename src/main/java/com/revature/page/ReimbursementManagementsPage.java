package com.revature.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class ReimbursementManagementsPage {
	private WebDriver driver;
	private WebElement homeButton;
	private WebElement manageButton;
	private WebElement userAccountButton;
	private WebElement profilesButton;
	private WebElement passwordsButton;
	private WebElement logoutButton;
	private WebElement cancelButton;
	private WebElement submitButton;
	private WebElement reimbIdField;
	private WebElement amountField;
	private WebElement descriptionField;
	private WebElement commentsField;
	private WebElement typeField;
	private WebElement authorNameField;
	private WebElement dateSubmittedField;
	private WebElement statusIdField;
	private WebElement receiptImage;
	
	public ReimbursementManagementsPage(WebDriver driver, String ersUserName, int ersUserId, int reimbId) {
		this.driver = driver;
		this.navigateTo(ersUserName, ersUserId, reimbId);
		
		this.homeButton = driver.findElement(By.id("homelink"));
		this.manageButton = driver.findElement(By.id("managelink"));
		this.userAccountButton = driver.findElement(By.id("navbarDropdownMenuLink"));
		this.profilesButton = driver.findElement(By.id("buttonProfile"));
		this.passwordsButton = driver.findElement(By.id("buttonPassword"));
		this.logoutButton = driver.findElement(By.id("buttonLogout"));
		
		this.cancelButton = driver.findElement(By.id("cancelbutton"));
		this.submitButton = driver.findElement(By.id("updatereimbbutton"));
		
		this.reimbIdField = driver.findElement(By.id("reimbId"));
		this.amountField = driver.findElement(By.id("reimbAmount"));
		this.descriptionField = driver.findElement(By.id("reimbDescription"));
		this.commentsField = driver.findElement(By.id("reimbComment"));
		this.typeField = driver.findElement(By.id("typeName"));
		this.authorNameField = driver.findElement(By.id("authorName"));
		this.dateSubmittedField = driver.findElement(By.id("reimbSubmitted"));
		this.statusIdField = driver.findElement(By.id("reimbStatusId"));
		this.receiptImage = driver.findElement(By.id("receiptImage"));

	}
	
	public void clickHomeButton() {
		this.homeButton.click();
	}
	
	public void clickManageButton() {
		this.manageButton.click();
	}
	
	public void clickProfilesButton() {
		this.userAccountButton.click();
		this.profilesButton.click();
	}
	
	public void clickPasswordsButton() {
		this.userAccountButton.click();
		this.passwordsButton.click();
	}
	
	public void clickLogoutButton() {
		this.userAccountButton.click();
		this.logoutButton.click();
	}
	
	public void clickCancelButton() {
		this.cancelButton.click();
	}
	
	public void clickSubmitButton() {
		this.submitButton.click();
	}
	
	public String getreimbId() {
		return this.reimbIdField.getAttribute("value");
	}
	
	public String getAmount() {
		return this.amountField.getAttribute("value");
	}
	
	public String getDescription() {
		return this.descriptionField.getAttribute("value");
	}
	
	public String getType() {
		return this.typeField.getAttribute("value");
	}
	
	public String getAuthorName() {
		return this.authorNameField.getAttribute("value");
	}

	public String getDateSubmitted() {
		return this.dateSubmittedField.getAttribute("value");
	}
	
	public boolean isImage() {
		return this.receiptImage.isDisplayed();
	}

	public void setComments(String reimbComment) {
		this.commentsField.clear();
		this.commentsField.sendKeys(reimbComment);
	}
	
	public void setStatusId(String userStatus) {
		Select dropdown = new Select(driver.findElement(By.id("reimbStatusId")));  
		dropdown.selectByVisibleText(userStatus);  
	}
	
	public void navigateTo(String ersUserName, int ersUserId, int reimbId) {
		this.driver.get("http://localhost:9001/html/reimbursementmanagements.html?ersUserId=" + ersUserId + "&ersUserName=" + ersUserName + "&reimbId=" + reimbId);
	}
}
