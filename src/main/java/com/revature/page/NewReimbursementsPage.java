package com.revature.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class NewReimbursementsPage {
	private WebDriver driver;
	private WebElement homeButton;
	private WebElement manageButton;
	private WebElement userAccountButton;
	private WebElement profilesButton;
	private WebElement passwordsButton;
	private WebElement logoutButton;
	private WebElement submitButton;
	private WebElement cancelButton;
	private WebElement amountField;
	private WebElement descriptionField;
	private WebElement typeIdField;
	private WebElement message;

	
	public NewReimbursementsPage(WebDriver driver, String ersUserName, int ersUserId) {
		this.driver = driver;
		this.navigateTo(ersUserName, ersUserId);
		
		this.homeButton = driver.findElement(By.id("homelink"));
		this.manageButton = driver.findElement(By.id("managelink"));
		this.userAccountButton = driver.findElement(By.id("navbarDropdownMenuLink"));
		this.profilesButton = driver.findElement(By.id("buttonProfile"));
		this.passwordsButton = driver.findElement(By.id("buttonPassword"));
		this.logoutButton = driver.findElement(By.id("buttonLogout"));
		
		this.submitButton = driver.findElement(By.id("addreimbbutton"));
		this.cancelButton = driver.findElement(By.id("cancelbutton"));
		this.amountField = driver.findElement(By.id("reimbAmount"));
		this.descriptionField = driver.findElement(By.id("reimbDescription"));
		this.typeIdField = driver.findElement(By.id("reimbTypeId"));
		this.message = driver.findElement(By.id("message"));

	}
	
	public void clickHomeButton() {
		this.homeButton.click();
	}
	
	public void clickManageButton() {
		this.manageButton.click();
	}
	
	public void clickProfilesButton() {
		this.userAccountButton.click();
		this.profilesButton.click();
	}
	
	public void clickPasswordsButton() {
		this.userAccountButton.click();
		this.passwordsButton.click();
	}
	
	public void clickLogoutButton() {
		this.userAccountButton.click();
		this.logoutButton.click();
	}
	
	public void clickSubmitButton() {
		this.submitButton.click();
	}
	
	public void clickCancelButton() {
		this.cancelButton.click();
	}
	
	public void setAmount(String amount) {
		this.amountField.clear();
		this.amountField.sendKeys(amount);
	}
	
	public String getAmount() {
		return this.amountField.getAttribute("value");
	}

	public void setDescription(String description) {
		this.descriptionField.clear();
		this.descriptionField.sendKeys(description);
	}
	
	public String getDescription() {
		return this.descriptionField.getAttribute("value");
	}
	
	public void setTypeId(String typeId) {
		Select dropdown = new Select(driver.findElement(By.id("reimbTypeId")));  
		dropdown.selectByVisibleText(typeId);  
	}
	
	public String getTypeId() {
		return this.typeIdField.getAttribute("value");
	}
	
	public String getMessage() {
		return this.message.getText();
	}
	
	public void navigateTo(String ersUserName, int ersUserId) {
		this.driver.get("http://localhost:9001/html/newreimbursements.html?ersUserId=" + ersUserId + "&ersUsername=" + ersUserName);
	}
}
