package com.revature.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class NewUsersPage {
	private WebDriver driver;
	private WebElement ersUserNameField;
	private WebElement userFirstNameField;
	private WebElement userLastNameField;
	private WebElement userEmailField;
	private WebElement userRoleIdField;
	private WebElement message;
	private WebElement submitButton;
	private WebElement cancelButton;
	
	public NewUsersPage(WebDriver driver) {
		this.driver = driver;
		this.navigateTo();
		
		this.ersUserNameField = driver.findElement(By.id("ersUserName"));
		this.userFirstNameField = driver.findElement(By.id("userFirstName"));
		this.userLastNameField = driver.findElement(By.id("userLastName"));
		this.userEmailField = driver.findElement(By.id("userEmail"));
		this.userRoleIdField = driver.findElement(By.id("userRoleId"));
		this.message = driver.findElement(By.id("message"));
		this.submitButton = driver.findElement(By.id("submitbutton"));
		this.cancelButton = driver.findElement(By.id("cancelbutton"));
	}
	
	public void setUserName(String ersUserName) {
		this.ersUserNameField.clear();
		this.ersUserNameField.sendKeys(ersUserName);
	}
	
	public String getUserName() {
		return this.ersUserNameField.getAttribute("value");
	}

	public void setFirstName(String userFirstName) {
		this.userFirstNameField.clear();
		this.userFirstNameField.sendKeys(userFirstName);
	}
	
	public String getFirstName() {
		return this.userFirstNameField.getAttribute("value");
	}
	
	public void setLastName(String userLastName) {
		this.userLastNameField.clear();
		this.userLastNameField.sendKeys(userLastName);
	}
	
	public String getLastName() {
		return this.userLastNameField.getAttribute("value");
	}
	
	public void setUserEmail(String userEmailName) {
		this.userEmailField.clear();
		this.userEmailField.sendKeys(userEmailName);
	}
	
	public String getUserEmail() {
		return this.userEmailField.getAttribute("value");
	}

	public void setRoleId(String userRole) {
		Select dropdown = new Select(driver.findElement(By.id("userRoleId")));  
		dropdown.selectByVisibleText(userRole);  
	}
	
	public String getRoleId() {
		return this.userRoleIdField.getAttribute("value");
	}
	
	public String getMessage() {
		return this.message.getText();
	}
	
	public void clickSubmitButton() {
		this.submitButton.click();
	}
	
	public void clickCancelButton() {
		this.cancelButton.click();
	}
	
	public void navigateTo() {
		this.driver.get("http://localhost:9001/html/newusers.html");
	}
}
