package com.revature.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {
	private WebDriver driver;
	private WebElement ersUserNameField;
	private WebElement ersPasswordField;
	private WebElement message;
	private WebElement loginButton;
	private WebElement newUserButton;
	private WebElement retrievePasswordButton;
	
	public LoginPage(WebDriver driver) {
		this.driver = driver;
		this.navigateTo();
		
		this.ersUserNameField = driver.findElement(By.id("ersUserName"));
		this.ersPasswordField = driver.findElement(By.id("ersPassword"));
		this.message = driver.findElement(By.id("message"));
		this.loginButton = driver.findElement(By.id("submitbutton"));
		this.newUserButton = driver.findElement(By.id("newUserButton"));
		this.retrievePasswordButton = driver.findElement(By.id("retrievePasswordButton"));
	}
	
	public void setUserName(String ersUserName) {
		this.ersUserNameField.clear();
		this.ersUserNameField.sendKeys(ersUserName);
	}
	
	public String getUserName() {
		return this.ersUserNameField.getAttribute("value");
	}
	
	public void setPassword(String ersPassword) {
		this.ersPasswordField.clear();
		this.ersPasswordField.sendKeys(ersPassword);
	}
	
	public String getPassword() {
		return this.ersPasswordField.getAttribute("value");
	}

	public String getMessage() {
		return this.message.getText();
	}
	
	public void clickLoginButton() {
		this.loginButton.click();
	}
	
	public void clickNewUserButton() {
		this.newUserButton.click();
	}
	
	public void clickRetrievePasswordButton() {
		this.retrievePasswordButton.click();
	}
	
	public void navigateTo() {
		this.driver.get("http://localhost:9001/html/login.html");
	}
}
