package com.revature.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class RetrievePasswordPage {
	private WebDriver driver;
	private WebElement ersUserNameField;
	private WebElement message;
	private WebElement submitButton;
	
	public RetrievePasswordPage(WebDriver driver) {
		this.driver = driver;
		this.navigateTo();
		
		this.ersUserNameField = driver.findElement(By.id("ersUserName"));
		this.message = driver.findElement(By.id("message"));
		this.submitButton = driver.findElement(By.id("submitbutton"));

	}
	
	public void setUserName(String ersUserName) {
		this.ersUserNameField.clear();
		this.ersUserNameField.sendKeys(ersUserName);
	}
	
	public String getUserName() {
		return this.ersUserNameField.getAttribute("value");
	}
	
	public String getMessage() {
		return this.message.getText();
	}
	
	public void navigateTo() {
		this.driver.get("http://localhost:9001/html/passwords.html");
	}
	
	public void clickSubmit() {
		this.submitButton.click();
	}

}
