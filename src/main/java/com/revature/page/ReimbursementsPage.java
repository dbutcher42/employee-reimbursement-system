package com.revature.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class ReimbursementsPage {
	private WebDriver driver;
	private WebElement homeButton;
	private WebElement manageButton;
	private WebElement userAccountButton;
	private WebElement profilesButton;
	private WebElement passwordsButton;
	private WebElement logoutButton;
	private WebElement closeButton;
	private WebElement chooseFile;
	private WebElement addReceiptButton;
	private WebElement reimbIdField;
	private WebElement amountField;
	private WebElement descriptionField;
	private WebElement commentsField;
	private WebElement typeField;
	private WebElement statusField;
	private WebElement authorNameField;
	private WebElement resolverNameField;
	private WebElement dateSubmittedField;
	private WebElement dateResolvedField;
	private WebElement receiptImage;
	private WebElement baseTable;
	
	public ReimbursementsPage(WebDriver driver, String ersUserName, int ersUserId, int reimbId) {
		this.driver = driver;
		this.navigateTo(ersUserName, ersUserId, reimbId);
		
		this.homeButton = driver.findElement(By.id("homelink"));
		this.manageButton = driver.findElement(By.id("managelink"));
		this.userAccountButton = driver.findElement(By.id("navbarDropdownMenuLink"));
		this.profilesButton = driver.findElement(By.id("buttonProfile"));
		this.passwordsButton = driver.findElement(By.id("buttonPassword"));
		this.logoutButton = driver.findElement(By.id("buttonLogout"));
		
		this.closeButton = driver.findElement(By.id("closeButton"));
		this.chooseFile = driver.findElement(By.id("receiptFile"));
		this.addReceiptButton = driver.findElement(By.id("updatereimbbutton"));
		
		this.reimbIdField = driver.findElement(By.xpath("//*[@id=\"reimbId\"]"));
		this.amountField = driver.findElement(By.xpath("//*[@id=\"reimbAmount\"]"));
		this.descriptionField = driver.findElement(By.id("reimbDescription"));
		this.commentsField = driver.findElement(By.id("reimbComment"));
		this.typeField = driver.findElement(By.xpath("//*[@id=\"typeName\"]"));
		this.statusField = driver.findElement(By.xpath("//*[@id=\"statusName\"]"));
		this.authorNameField = driver.findElement(By.xpath("//*[@id=\"authorName\"]"));
		this.resolverNameField = driver.findElement(By.xpath("//*[@id=\"resolverName\"]"));
		this.dateSubmittedField = driver.findElement(By.xpath("//*[@id=\"reimbSubmitted\"]"));
		this.dateResolvedField = driver.findElement(By.xpath("//*[@id=\"reimbResolved\"]"));
		this.receiptImage = driver.findElement(By.id("receiptImage"));
		this.baseTable = driver.findElement(By.id("reimbTable"));

	}
	
	public void clickHomeButton() {
		this.homeButton.click();
	}
	
	public void clickManageButton() {
		this.manageButton.click();
	}
	
	public void clickProfilesButton() {
		this.userAccountButton.click();
		this.profilesButton.click();
	}
	
	public void clickPasswordsButton() {
		this.userAccountButton.click();
		this.passwordsButton.click();
	}
	
	public void clickLogoutButton() {
		this.userAccountButton.click();
		this.logoutButton.click();
	}
	
	public void clickCloseButton() {
		this.closeButton.click();
	}
	
	public void clickChooseFile(String imageFile) {
		this.chooseFile.sendKeys(imageFile);
		this.clickAddReceiptButton();
	}
	
	public void clickAddReceiptButton() {
		this.addReceiptButton.click();
	}
	
	public String getreimbId() {
		return this.reimbIdField.getText();
	}
	
	public String getAmount() {
		return this.amountField.getText();
	}
	
	public String getDescription() {
		return this.descriptionField.getAttribute("value");
	}
	
	public String getComments() {
		return this.commentsField.getAttribute("value");
	}
	
	public String getType() {
		return this.typeField.getText();
	}
	
	public String getStatus() {
		return this.statusField.getText();
	}
	
	public String getAuthorName() {
		return this.authorNameField.getText();
	}

	public String getResolverName() {
		return this.resolverNameField.getText();
	}
	
	public String getDateSubmitted() {
		return this.dateSubmittedField.getText();
	}
	
	public String getDateResolved() {
		return this.dateResolvedField.getText();
	}
	
	public boolean isImage() {
		return this.receiptImage.isDisplayed();
	}
	
	public void navigateTo(String ersUserName, int ersUserId, int reimbId) {
		this.driver.get("http://localhost:9001/html/reimbursements.html?ersUserId=" + ersUserId + "&ersUserName=" + ersUserName + "&reimbId=" + reimbId);
	}
}
