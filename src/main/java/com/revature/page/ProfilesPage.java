package com.revature.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class ProfilesPage {
	private WebDriver driver;
	private WebElement homeButton;
	private WebElement manageButton;
	private WebElement userAccountButton;
	private WebElement profilesButton;
	private WebElement passwordsButton;
	private WebElement logoutButton;
	private WebElement updateButton;
	private WebElement cancelButton;
	private WebElement userFirstNameField;
	private WebElement userLastNameField;
	private WebElement userEmailField;
	private WebElement userRoleIdField;
	private WebElement message;

	
	public ProfilesPage(WebDriver driver, String ersUserName, int ersUserId) {
		this.driver = driver;
		this.navigateTo(ersUserName, ersUserId);
		
		this.homeButton = driver.findElement(By.id("homelink"));
		this.manageButton = driver.findElement(By.id("managelink"));
		this.userAccountButton = driver.findElement(By.id("navbarDropdownMenuLink"));
		this.profilesButton = driver.findElement(By.id("buttonProfile"));
		this.passwordsButton = driver.findElement(By.id("buttonPassword"));
		this.logoutButton = driver.findElement(By.id("buttonLogout"));
		
		this.updateButton = driver.findElement(By.id("submitbutton"));
		this.cancelButton = driver.findElement(By.id("cancelbutton"));
		this.userFirstNameField = driver.findElement(By.id("userFirstName"));
		this.userLastNameField = driver.findElement(By.id("userLastName"));
		this.userEmailField = driver.findElement(By.id("userEmail"));
		this.userRoleIdField = driver.findElement(By.id("userRoleId"));
		this.message = driver.findElement(By.id("message"));

	}
	
	public void clickHomeButton() {
		this.homeButton.click();
	}
	
	public void clickManageButton() {
		this.manageButton.click();
	}
	
	public void clickProfilesButton() {
		this.userAccountButton.click();
		this.profilesButton.click();
	}
	
	public void clickPasswordsButton() {
		this.userAccountButton.click();
		this.passwordsButton.click();
	}
	
	public void clickLogoutButton() {
		this.userAccountButton.click();
		this.logoutButton.click();
	}
	
	public void clickUpdateButton() {
		this.updateButton.click();
	}
	
	public void clickCancelButton() {
		this.cancelButton.click();
	}
	
	public void setFirstName(String userFirstName) {
		this.userFirstNameField.clear();
		this.userFirstNameField.sendKeys(userFirstName);
	}
	
	public String getFirstName() {
		return this.userFirstNameField.getAttribute("value");
	}
	
	public void setLastName(String userLastName) {
		this.userLastNameField.clear();
		this.userLastNameField.sendKeys(userLastName);
	}
	
	public String getLastName() {
		return this.userLastNameField.getAttribute("value");
	}
	
	public void setUserEmail(String userEmailName) {
		this.userEmailField.clear();
		this.userEmailField.sendKeys(userEmailName);
	}
	
	public String getUserEmail() {
		return this.userEmailField.getAttribute("value");
	}

	public void setRoleId(String userRole) {
		Select dropdown = new Select(driver.findElement(By.id("userRoleId")));  
		dropdown.selectByVisibleText(userRole);  
	}
	
	public String getRoleId() {
		return this.userRoleIdField.getAttribute("value");
	}
	
	public String getMessage() {
		return this.message.getText();
	}
	
	public void navigateTo(String ersUserName, int ersUserId) {
		this.driver.get("http://localhost:9001/html/profiles.html?ersUserId=" + ersUserId + "&ersUsername=" + ersUserName);
	}
}
