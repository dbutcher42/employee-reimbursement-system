package com.revature.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class FinanceReimbursementsPage {
	private WebDriver driver;
	private WebElement homeButton;
	private WebElement manageButton;
	private WebElement userAccountButton;
	private WebElement profilesButton;
	private WebElement passwordsButton;
	private WebElement logoutButton;
	private WebElement allButton;
	private WebElement pendingButton;
	private WebElement approvedButton;
	private WebElement deniedButton;
	private WebElement baseTable;
	private WebElement reimbId;
	private WebElement reimbAmount;
	private WebElement reimbSubmitted;
	private WebElement reimbResolved;
	private WebElement typeName;
	private WebElement statusName;
	private WebElement actionButton;
	
	public FinanceReimbursementsPage(WebDriver driver, String ersUserName, int ersUserId) {
		this.driver = driver;
		this.navigateTo(ersUserName, ersUserId);
		
		this.homeButton = driver.findElement(By.id("homelink"));
		this.manageButton = driver.findElement(By.id("managelink"));
		this.userAccountButton = driver.findElement(By.id("navbarDropdownMenuLink"));
		this.profilesButton = driver.findElement(By.id("buttonProfile"));
		this.passwordsButton = driver.findElement(By.id("buttonPassword"));
		this.logoutButton = driver.findElement(By.id("buttonLogout"));
		
		this.allButton = driver.findElement(By.id("buttonSelectAll"));
		this.pendingButton = driver.findElement(By.id("buttonSelectPending"));
		this.approvedButton = driver.findElement(By.id("buttonSelectApproved"));
		this.deniedButton = driver.findElement(By.id("buttonSelectDenied"));
		this.baseTable = driver.findElement(By.id("reimbTable"));

	}
	
	public void clickHomeButton() {
		this.homeButton.click();
	}
	
	public void clickManageButton() {
		this.manageButton.click();
	}
	
	public void clickProfilesButton() {
		this.userAccountButton.click();
		this.profilesButton.click();
	}
	
	public void clickPasswordsButton() {
		this.userAccountButton.click();
		this.passwordsButton.click();
	}
	
	public void clickLogoutButton() {
		this.userAccountButton.click();
		this.logoutButton.click();
	}
	
	public void clickAllButton() {
		this.allButton.click();
	}
	
	public void clickPendingButton() {
		this.pendingButton.click();
	}
	
	public void clickApprovedButton() {
		this.approvedButton.click();
	}
	
	public void clickDeniedButton() {
		this.deniedButton.click();
	}
	
	public String getId(int row) {
		WebElement tableRow = baseTable.findElement(By.xpath("//*[@id=\"tableBody\"]/tr[" + row + "]"));
		reimbId = tableRow.findElement(By.xpath("//*[@id=\"tableBody\"]/tr[" + row + "]/td[1]"));
		return reimbId.getText();
	}
	
	public String getAmount(int row) {
		WebElement tableRow = baseTable.findElement(By.xpath("//*[@id=\"tableBody\"]/tr[" + row + "]"));
		reimbAmount = tableRow.findElement(By.xpath("//*[@id=\"tableBody\"]/tr[" + row + "]/td[2]"));
		return reimbAmount.getText();
	}
	
	public String getDateSubmitted(int row) {
		WebElement tableRow = baseTable.findElement(By.xpath("//*[@id=\"tableBody\"]/tr[" + row + "]"));
		reimbSubmitted = tableRow.findElement(By.xpath("//*[@id=\"tableBody\"]/tr[" + row + "]/td[3]"));
		return reimbSubmitted.getText();
	}
	
	public String getDateResolved(int row) {
		WebElement tableRow = baseTable.findElement(By.xpath("//*[@id=\"tableBody\"]/tr[" + row + "]"));
		reimbResolved = tableRow.findElement(By.xpath("//*[@id=\"tableBody\"]/tr[" + row + "]/td[4]"));
		return reimbResolved.getText();
	}
	
	public String getType(int row) {
		WebElement tableRow = baseTable.findElement(By.xpath("//*[@id=\"tableBody\"]/tr[" + row + "]"));
		typeName = tableRow.findElement(By.xpath("//*[@id=\"tableBody\"]/tr[" + row + "]/td[5]"));
		return typeName.getText();
	}
	
	public String getStatus(int row) {
		WebElement tableRow = baseTable.findElement(By.xpath("//*[@id=\"tableBody\"]/tr[" + row + "]"));
		statusName = tableRow.findElement(By.xpath("//*[@id=\"tableBody\"]/tr[" + row + "]/td[6]"));
		return statusName.getText();
	}
	
	public void clickActionButton(int row) {
		WebElement tableRow = baseTable.findElement(By.xpath("//*[@id=\"tableBody\"]/tr[" + row + "]"));
		actionButton = tableRow.findElement(By.xpath("//*[@id=\"tableBody\"]/tr[" + row + "]/td[7]"));
		actionButton.click();
	}
	
	public void navigateTo(String ersUserName, int ersUserId) {
		this.driver.get("http://localhost:9001/html/financereimbursements.html?ersUserId=" + ersUserId + "&ersUsername=" + ersUserName);
	}
}
