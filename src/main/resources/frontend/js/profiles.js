/**
 * 
 */

let ersUserId;
let ersUserName;
let userName;
let userNameJson;
let userRoleId;
let userFirstName;
let userLastName;
let oldEmail;

window.onload = function() {
	let queryString = window.location.search;
	ersUserId = getQueryVariable('ersUserId');
	ersUserName = getQueryVariable('ersUsername');
	let userNameObj = {
		ersUserName: userName  
	};
	
	userNameJson = JSON.stringify(ersUserName);
	getUser();

};

function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == variable) {
            return decodeURIComponent(pair[1]);
        }
    }
    console.log('Query variable %s not found', variable);
};

function submitForm() {
	event.preventDefault();

	let form = document.getElementById("updateUserForm");
	let formData = new FormData(form);
	formData.append('ersUserId', ersUserId);
	formData.append('oldEmail', oldEmail);
	
	let fname = document.getElementById("userFirstName").value;
	let lname = document.getElementById("userLastName").value;
	let ename = document.getElementById("userEmail").value;
	if (fname=="" || lname== "" || ename == "") {
		putMessage("YOU MUST POPULATE ALL FIELDS");
	} else {
		fetch(`/users/${ersUserId}/profiles`,
	    {
	        body: formData,
   		    method: "patch"
    	}).then(function(response) {
			if (response.status == 201) {
				window.location.href = `/html/home.html?ersUsername=${ersUserName}`;
			} else {
				putMessage("YOUR EMAIL IS ALREADY REGISTERED, USE ANOTHER");
			}
		});
	};
};

function getUser() {
			
	let xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if(xhttp.readyState==4 && xhttp.status==200) {
			let user = JSON.parse(xhttp.responseText);
			ersUserId = user.ersUserId;
			userFirstName = user.userFirstName;
			userLastName = user.userLastName;
			userRoleId = user.userRoleId;
			userEmail = user.userEmail;
			oldEmail = userEmail;
			
			if (userRoleId == 2) {
				tempLink = document.getElementById("managelink");
				document.getElementById('managelink').removeAttribute('hidden');
			};
			
			document.getElementById("buttonProfile").addEventListener('click', editProfile);
			document.getElementById("buttonPassword").addEventListener('click', editPassword);
			document.getElementById("managelink").addEventListener('click', manageReimbursement);
			document.getElementById("homelink").addEventListener('click', gohome);
			
			document.getElementById("userFirstName").setAttribute("value", userFirstName);
			document.getElementById("userLastName").setAttribute("value", userLastName);
			document.getElementById("userEmail").setAttribute("value", userEmail);
			document.getElementById("userRoleId").selectedIndex = userRoleId-1;

			document.getElementById("cancelbutton").addEventListener('click', gohome);
			document.getElementById("submitbutton").addEventListener('click', submitForm);
			
		};
	};
	xhttp.open("GET", `/users/namedusers/${ersUserName}`);
	xhttp.send();
};	

function editProfile() {
	window.location.href = `/html/profiles.html?ersUserId=${ersUserId}&ersUsername=${ersUserName}`;
};

function editPassword() {
	window.location.href = `/html/newpasswords.html?ersUserId=${ersUserId}&ersUsername=${ersUserName}`;
};

function manageReimbursement() {
	window.location.href = `/html/financereimbursements.html?ersUserId=${ersUserId}&ersUsername=${ersUserName}`;
};

function gohome() {
	window.location.href = `/html/home.html?ersUserId=${ersUserId}&ersUsername=${ersUserName}`;
};

function putMessage(message) {
	document.getElementById("message").innerText=message;
};