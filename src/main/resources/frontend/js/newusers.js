/**
 * 
 */


userNameUsed = false;
emailUsed = false;

window.onload = function() {
	document.getElementById("ersUserName").addEventListener('change', checkUserName);
	document.getElementById("userEmail").addEventListener('change', checkEmail);
	document.getElementById("submitbutton").addEventListener('click', submitForm);
	document.getElementById("cancelbutton").addEventListener('click', gologin);
};

function putMessage(message) {
	document.getElementById("message").innerText=message;
};

function checkUserName() {
	
	let userName = document.getElementById('ersUserName').value;
	fetch(`/users/usernames/${userName}`)
		.then(function(response) { //this is my function for success
			if (response.status == 250) {
				userNameUsed = true;
				putMessage("YOUR USER NAME IS ALREADY TAKEN, PICK ANOTHER");
			} else {
				userNameUsed = false;
				putMessage("Your temporary password will be sent to your email address");
			}
		}, function(){ //this function is for failure

		});
};

function checkEmail() {
	
	let userEmail = document.getElementById('userEmail').value;
	
	fetch(`/users/emails/${userEmail}`)
	.then(function(response) { //this is my function for success
		if (response.status == 250) {
			emailUsed = true;
			putMessage("YOUR EMAIL IS ALREADY REGISTERED, USE ANOTHER");
		} else {
			emailUsed = false;
			putMessage("Your temporary password will be sent to your email address");
		}
	}, function(){ //this function is for failure

	});

};

function submitForm() {
	event.preventDefault();
	
	let form = document.getElementById("newUserForm");
	let formData = new FormData(form);

	checkUserName();
	if (userNameUsed) {
		putMessage("YOUR USER NAME IS ALREADY TAKEN, PICK ANOTHER");
	} else {
		checkEmail();
		if (emailUsed) {
			putMessage("YOUR EMAIL IS ALREADY REGISTERED, USE ANOTHER");
		} else {
			let uname = document.getElementById("ersUserName").value;
			let fname = document.getElementById("userFirstName").value;
			let lname = document.getElementById("userLastName").value;
			let ename = document.getElementById("userEmail").value;
			if (uname == "" || fname=="" || lname== "" || ename == "") {
				putMessage("YOU MUST POPULATE ALL FIELDS");
			} else {
				fetch("/users",
		   	 	{
		        	body: formData,
		        	method: "post"
	    		}).then(function(response) {
					if (response.status == 201) {
						window.location.href = "/html/login.html";
					} else {
						putMessage("USER NAME OR EMAIL ALREADY TAKEN");
					};
				});					
			};

		};
	};


};

function gologin() {
	window.location.href = "/html/login.html";
};

