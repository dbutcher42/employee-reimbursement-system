/**
 * 
 */

window.onload = function() {
	document.getElementById("submitbutton").addEventListener('click', submitForm);
	document.getElementById("cancelbutton").addEventListener('click', gologin);
};

function putMessage(message) {
	document.getElementById("message").innerText=message;
};

function submitForm() {
	event.preventDefault();

	let userName = document.getElementById('ersUserName').value;

	fetch(`/users/${userName}/passwords`)
		.then(function(response) { //this is my function for success
			if (response.status == 250) {
				putMessage("You will be redirected to the login page.");
				window.location.href = "/html/login.html";
			} else {
				putMessage("Your user name is invalid");
			}
		}, function(){ //this function is for failure

		});
};

function gologin() {
	window.location.href = "/html/login.html";
};