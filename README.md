# Project Employee Reimbursement System
The files in this repo were copied from here for showing it on my portfolio. That is why you will not see any commit history.

This is a simple employee reimbursement system.  It allows employees to register and login to the system where they can add reimbursement requests and view past requests.  Managers can view the requests either approve or decline requests.

**Features**

- Login functionality
- Registration functionality with welcome email sent.
- Password recovery via email.
- Employees can view past reimbursements, add new reimbursements with receipt images, and enter new reimbursement requests.  Employees can change their profile information and also their passwords.
- Managers have the same capabilities as employees.  They can also manage requests for employees other than themselves, viewing pending requests and approving or denying them.


**Technologies Used**
- Java 8
- HTML
- CSS
- JavaScript
- Javalin
- JavaMail API
- JDBC
- SQL
- RDS
- Maven
- AJAX
- Git
- Spring Test Suite

**Getting Started**

This project requires the project be loaded in Spring Tool Suite 4.  It is not hosted anywhere.  Under src/main/java find the package com.revature and run MainDriver.java.  Once that is running you will need to go to your browser and go to http://localhost:9001/html/login.html.  

**Usage**

Screen images are available in screenshots folder.  Unfortunately the emails are being sent to my personal email box so you cannot really register as a new user as passwords are randomly generated and sent out via email.  Log into the employee screen using "Manager1" and "password".  You can filter the reimbursements shown by status.  You can add a new reimbursement.  You can switch to the manager reimbursement screen and view reimbursements for all employees other than yourself.  You can then click on manage of a pending request and go to the manage reimbursement screen where you can approve or deny the request.  It is possible to change your user profile information and your password.

